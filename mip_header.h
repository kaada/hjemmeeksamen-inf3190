#define MAX_EVENTS 10
#define MAX_INTERFACES 4
#define MAX_TABLE_SIZE 10
#define MAX_FILE_SIZE 65535
#define MAX_DATA_PER_PACKET 1492
#define MAX_APP_CONNECTIONS 5
#define MAX_FILE_SIZE 65535


struct ethernet_frame {
  uint8_t dest_macaddr[6];
  uint8_t source_macaddr[6];
  uint16_t protocol;
  char msg[];
} __attribute__((packed));

struct network_datagram{
  uint32_t network_header;
  char msg[];
} __attribute__((packed));

struct epoll_control {
  int epoll_fd;
  int client_accept_fd;
  int unix_sfd;
  int routing_fd;
  int forwarding_fd;
  int accept_rawsockets[5];
  struct epoll_event events[MAX_EVENTS];
};

struct mip_arp_info{
  uint8_t source_macaddr[6];
  uint8_t dest_macaddr[6];
  int socket_fd;
  int source_mip;
  int dest_mip;
};


struct pend_msg_from_client{
  int mip_addr;
  int source_mip;
  int routing; //if routing == 1, then TRA = 010
  size_t length;
  char msg[];
};

struct pending_msg_from_client_queue_struct{
  int pending_msg_bool;
  struct pend_msg_from_client* pending_msg_from_client;
};

/*
  This is the struct that is used in the routing table.
*/
struct dest_outgoing_cost{
  int outgoing_mip; //via
  int cost;
  int dest_mip[MAX_INTERFACES];
};

struct outgoing_and_cost{
  int outgoing_mip;
  int cost;
};

/*
  This is the struct that is used in the distance table.
*/
struct distance_table_struct{
  int dest_mip[MAX_INTERFACES];
  time_t last_received_from_host;
  int neighbor; //0 == ikke, 1 == ja
  struct outgoing_and_cost via_and_cost[MAX_INTERFACES];
};

/*
  A struct that contains info of epoll and its sockets. For routing_server
*/
struct epoll_control_router{
  int epoll_fd;
  int accept_forwarding_fd;
  int accept_routing_fd;
  int forwarding_fd;
  int routing_fd;
  struct epoll_event events[2];
};

struct app_info{
  int port_number;
  int dest_mip;
};

struct file_struct{
  uint16_t file_size;
  char file[MAX_FILE_SIZE];
};

struct transport_packet{
  uint32_t miptp_header;
  char data[];
};

struct transport_packet_info{
  int length;
  struct transport_packet* transport_packet;
};

//used in tranport daemon to have all the info of a connection
struct connected_apps_struct{
  int fd;
  int port_number;
  int destination_mip;

  time_t package_sent;
  int ack_nr;
  int receiving_end;
  int teardown_sent_counter;

  uint16_t file_size;
  struct transport_packet_info packets[(MAX_FILE_SIZE/MAX_DATA_PER_PACKET)+1];
};

struct epoll_control_transport{
  int epoll_fd;
  int mip_daemon_fd;
  int listen_sock;
  struct connected_apps_struct connected_apps[MAX_APP_CONNECTIONS];
  struct epoll_event events[MAX_EVENTS];
};
