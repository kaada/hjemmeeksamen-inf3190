#include <sys/epoll.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/types.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <inttypes.h>
#include <time.h>
#include <limits.h>
#include "mip_header.h"


#define SEQ_NR_MASK 65535
#define TIMEOUT_SYNACK 6

int debugmode = 0;


/*
  This function prints a debug message sent in by the parameter "msg" if the program
  is running in debug mode.

  Parameters:
    msg: The debug message
    port_number: the port number where the action happend
*/
void debug_print(char* msg, int port_number){
  if(debugmode == 1){
    printf("-- Port: %d --  %s", port_number, msg);
  }
}

/*
  This function prints a debug message when sending a message from client
  which contains info of which packet and on which port if the program is
  running in debug mode.

  Parameters:
    ep_ctrl: a pointer to the epoll_control struct that contains info of the epoll
    index: index in the connected_apps-array where the info of this packet is stored
*/
void debug_print_sending_client(struct epoll_control_transport* ep_ctrl, int index, int package_nr){
  if(debugmode == 1){
    if(ep_ctrl->connected_apps[index].ack_nr+1 > (ep_ctrl->connected_apps[index].file_size/1492 + 1)){
      printf("-- Port: %d --  Sending package %i to MIP-addr %i on port %i | Window: [%i -> %i]\n", ep_ctrl->connected_apps[index].port_number, package_nr,  ep_ctrl->connected_apps[index].destination_mip, ep_ctrl->connected_apps[index].port_number, (ep_ctrl->connected_apps[index].file_size/1492 + 1), (ep_ctrl->connected_apps[index].file_size/1492 + 1));
    } else if(ep_ctrl->connected_apps[index].ack_nr+10 > (ep_ctrl->connected_apps[index].file_size/1492 + 1)){
      printf("-- Port: %d --  Sending package %i to MIP-addr %i on port %i | Window: [%i -> %i]\n", ep_ctrl->connected_apps[index].port_number, package_nr,  ep_ctrl->connected_apps[index].destination_mip, ep_ctrl->connected_apps[index].port_number, ep_ctrl->connected_apps[index].ack_nr+1, (ep_ctrl->connected_apps[index].file_size/1492 + 1));
    } else{
      printf("-- Port: %d --  Sending package %i to MIP-addr %i on port %i | Window: [%i -> %i]\n", ep_ctrl->connected_apps[index].port_number, package_nr,  ep_ctrl->connected_apps[index].destination_mip, ep_ctrl->connected_apps[index].port_number, ep_ctrl->connected_apps[index].ack_nr+1, ep_ctrl->connected_apps[index].ack_nr+10);
    }
  }
}

/*
  This function prints a debug message when receiving an ack in the client
  which contains info of which packet and on which port if the program is
  running in debug mode.

  Parameters:
    ep_ctrl: a pointer to the epoll_control struct that contains info of the epoll
    index: index in the connected_apps-array where the info of this packet is stored
*/
void debug_print_ack_client(struct epoll_control_transport* ep_ctrl, int index){
  if(debugmode == 1){
    if(ep_ctrl->connected_apps[index].ack_nr+1 > (ep_ctrl->connected_apps[index].file_size/1492 + 1)){
      printf("-- Port: %d --  Has received an ack on package %i | Window: [%i -> %i]\n", ep_ctrl->connected_apps[index].port_number, ep_ctrl->connected_apps[index].ack_nr, (ep_ctrl->connected_apps[index].file_size/1492 + 1), (ep_ctrl->connected_apps[index].file_size/1492 + 1));
    } else if(ep_ctrl->connected_apps[index].ack_nr+10 > (ep_ctrl->connected_apps[index].file_size/1492 + 1)){
      printf("-- Port: %d --  Has received an ack on package %i | Window: [%i -> %i]\n", ep_ctrl->connected_apps[index].port_number, ep_ctrl->connected_apps[index].ack_nr, ep_ctrl->connected_apps[index].ack_nr+1, (ep_ctrl->connected_apps[index].file_size/1492 + 1));
    } else{
      printf("-- Port: %d --  Has received an ack on package %i | Window: [%i -> %i]\n", ep_ctrl->connected_apps[index].port_number, ep_ctrl->connected_apps[index].ack_nr, ep_ctrl->connected_apps[index].ack_nr+1, ep_ctrl->connected_apps[index].ack_nr+10);
    }
  }
}

/*
  This function prints a debug message when the server side receives a package
  if the program is running in debug mode.

  Parameters:
    package_nr: which package that is received
    port_nr: which port the package was sent to
*/
void debug_print_recv_server(int package_nr, int port_nr){
  if(debugmode == 1){
    printf("-- Port: %d --  Has received package %d\n", port_nr, package_nr);
  }
}

/*
  This function prints a debug message when the server side sends acks on received
  packages if the program is running in debug mode.

  Parameters:
    ep_ctrl: a pointer to the epoll_control struct that contains info of the epoll
    index: index in the connected_apps-array where the info of this packet is stored
*/
void debug_print_send_ack(struct epoll_control_transport* ep_ctrl, int index){
  if(debugmode == 1){
    printf("-- Port: %d --  Sending ack on package %d\n", ep_ctrl->connected_apps[index].port_number, ep_ctrl->connected_apps[index].ack_nr);
  }
}

/*
  This function sends packages that are stored in "transport_packet" structs to the mip_daemon.

  Parameters:
    ep_ctrl: a pointer to the epoll_control struct that contains info of the epoll
    index: index in the connected_apps-array where the info of this packet and connection is stored
    start_index: index to the first package in the "transport_packet_info" struct array
      that is going to be sent
    num_elem_to_send: a number of how many packages that are going to be sent

  Return values:
    It returns -1 on errors and 1 on success.
*/
int send_packages(struct epoll_control_transport* ep_ctrl, int index, int start_index, int num_elem_to_send){
  int i;
  int dest_mip = ep_ctrl->connected_apps[index].destination_mip;
  for(i = start_index; i<(MAX_FILE_SIZE/MAX_DATA_PER_PACKET)+1 && ep_ctrl->connected_apps[index].packets[i].length != 0 && i<ep_ctrl->connected_apps[index].ack_nr + 1 + num_elem_to_send; i++){
    debug_print_sending_client(ep_ctrl, index, i);
    char buffer[1496] = {0};
    memcpy(buffer, ep_ctrl->connected_apps[index].packets[i].transport_packet, ep_ctrl->connected_apps[index].packets[i].length);
    size_t length = ep_ctrl->connected_apps[index].packets[i].length;

    struct iovec iov[3];

    iov[0].iov_base = buffer;
    iov[0].iov_len = sizeof(buffer);

    iov[1].iov_base = &dest_mip;
    iov[1].iov_len = sizeof(dest_mip);

    iov[2].iov_base = &length;
    iov[2].iov_len = sizeof(size_t);

    struct msghdr message_struct;
    memset(&message_struct, 0, sizeof(message_struct));
    message_struct.msg_iov = iov;
    message_struct.msg_iovlen = 3;

    //to not let the OS reorder packets
    usleep(10);

    ep_ctrl->connected_apps[index].package_sent = time(NULL);
    ssize_t rv = sendmsg(ep_ctrl->mip_daemon_fd, &message_struct, 0);
    if(rv == -1){
      perror("main(): sendmsg()");
      return -1;
    }


  }

  return 1;
}

/*
  This function sends acks on the packets that has arrived correct.

  Parameters:
    ep_ctrl: a pointer to the epoll_control struct that contains info of the epoll
    acknr: the sequence number on the package that's going to be ack'd.
    port_nr: the port number it arrived/was sent to
    dest_mip: the mip address to the host that sent the package

  Return values:
    It returns -1 on errors and 1 on success.
*/
int send_ack(struct epoll_control_transport* ep_ctrl, int acknr, int port_nr, int dest_mip){
  char buffer[1496] = {0};
  struct transport_packet* tp = malloc(sizeof(struct transport_packet));
  memset(tp, 0, sizeof(struct transport_packet));
  tp->miptp_header = acknr | port_nr<<16;
  memcpy(buffer, tp, sizeof(struct transport_packet));

  size_t length = 4;
  struct iovec iov[3];

  iov[0].iov_base = buffer;
  iov[0].iov_len = sizeof(buffer);

  iov[1].iov_base = &dest_mip;
  iov[1].iov_len = sizeof(dest_mip);

  iov[2].iov_base = &length;
  iov[2].iov_len = sizeof(size_t);

  struct msghdr message_struct;
  memset(&message_struct, 0, sizeof(message_struct));
  message_struct.msg_iov = iov;
  message_struct.msg_iovlen = 3;

  ssize_t rv = sendmsg(ep_ctrl->mip_daemon_fd, &message_struct, 0);
  free(tp);
  if(rv == -1){
    perror("main(): sendmsg()");
    return -1;
  }
  return 1;
}

/*
  This function sends a SYN-message to the server side, which also contains
  the size of the file.

  Parameters:
    ep_ctrl: a pointer to the epoll_control struct that contains info of the epoll
    index: index in the connected_apps-array where the info of this connection is stored

  Return values:
    It returns -1 on errors and 1 on success.
*/
int establish_connection(struct epoll_control_transport* ep_ctrl, int index){
  int dest_mip = ep_ctrl->connected_apps[index].destination_mip;
  char buffer[1496] = {0};
  memcpy(buffer, ep_ctrl->connected_apps[index].packets[0].transport_packet, ep_ctrl->connected_apps[index].packets[0].length);
  size_t length = ep_ctrl->connected_apps[index].packets[0].length;
  struct iovec iov[3];

  iov[0].iov_base = buffer;
  iov[0].iov_len = sizeof(buffer);

  iov[1].iov_base = &dest_mip;
  iov[1].iov_len = sizeof(dest_mip);

  iov[2].iov_base = &length;
  iov[2].iov_len = sizeof(size_t);

  struct msghdr message_struct;
  memset(&message_struct, 0, sizeof(message_struct));
  message_struct.msg_iov = iov;
  message_struct.msg_iovlen = 3;

  ep_ctrl->connected_apps[index].ack_nr = -1;
  ep_ctrl->connected_apps[index].package_sent = time(NULL);

  debug_print("Sending SYN-message to the server side\n", ep_ctrl->connected_apps[index].port_number);
  ssize_t rv = sendmsg(ep_ctrl->mip_daemon_fd, &message_struct, 0);
  if(rv == -1){
    perror("main(): sendmsg()");
    return -1;
  }

  return 1;
}

/*
  This function sends teardown message, so that the connection between the two hosts
  can be disconnected.

  Parameters:
    ep_ctrl: a pointer to the epoll_control struct that contains info of the epoll
    index: index in the connected_apps-array where the info of this connection is stored

  Return values:
    It returns -1 on errors and 1 on success.
*/
int send_teardown_msg(struct epoll_control_transport* ep_ctrl, int index){
  int dest_mip = ep_ctrl->connected_apps[index].destination_mip;
  char buffer[1496] = {0};
  struct transport_packet* tp = malloc(sizeof(struct transport_packet) + 4);
  memset(tp, 0, sizeof(struct transport_packet));
  tp->miptp_header = SEQ_NR_MASK | ep_ctrl->connected_apps[index].port_number<<16;
  uint32_t t = 65535;
  //puts data in because the receiving end has to distinguish between a teardown ack and a teardown message
  memcpy(tp->data, &t, 4);
  memcpy(buffer, tp, 8);
  size_t length = 8; //header + data
  struct iovec iov[3];

  iov[0].iov_base = buffer;
  iov[0].iov_len = sizeof(buffer);

  iov[1].iov_base = &dest_mip;
  iov[1].iov_len = sizeof(dest_mip);

  iov[2].iov_base = &length;
  iov[2].iov_len = sizeof(size_t);

  struct msghdr message_struct;
  memset(&message_struct, 0, sizeof(message_struct));
  message_struct.msg_iov = iov;
  message_struct.msg_iovlen = 3;

  ep_ctrl->connected_apps[index].package_sent = time(NULL);
  ep_ctrl->connected_apps[index].teardown_sent_counter++;

  debug_print("Sending teardown-message to the server side\n", ep_ctrl->connected_apps[index].port_number);
  ssize_t rv = sendmsg(ep_ctrl->mip_daemon_fd, &message_struct, 0);
  if(rv == -1){
    perror("main(): sendmsg()");
    free(tp);
    return -1;
  }

  free(tp);
  return 1;
}

/*
  This function receives packages from the mip daemon, and handles depending what's
  sent. It has the responsibility to call other functions if that's necessary.

  Parameters:
    ep_ctrl: a pointer to the epoll_control struct that contains info of the epoll

  Return values:
    0 and 1: on success, where it has handled the package correct
    -1: on errors
    -2: if the mipdaemon has disconnected
    -3: if it has arrived an syn-ack that it not was supposed to
*/
int recv_packages(struct epoll_control_transport* ep_ctrl){
  char buf[1496] = {0};
  int mip_adr_to_sender = 0;
  int mip_payload_length = 0;
  struct iovec iov[3];
  iov[0].iov_base = buf;
  iov[0].iov_len = sizeof(buf);

  iov[1].iov_base = &mip_adr_to_sender;
  iov[1].iov_len = sizeof(mip_adr_to_sender);

  iov[2].iov_base = &mip_payload_length;
  iov[2].iov_len = sizeof(mip_payload_length);

  struct msghdr message;
  memset(&message, 0, sizeof(message));
  message.msg_iov = iov;
  message.msg_iovlen = 3;


  ssize_t rv = recvmsg(ep_ctrl->mip_daemon_fd, &message, 0);
  if(rv == -1){
    perror("main(): epoll_event(): recv_packages(): recvmsg()");
    return -1;
  } else if(rv == 0){
    fprintf(stderr, "The mip daemon has performed an orderly shutdown\n");
    close(ep_ctrl->mip_daemon_fd);
    return -2;
  }



  struct transport_packet* t_p = (struct transport_packet*)buf;

  //the incoming port number
  int incoming_pn = (t_p->miptp_header & 1073676288)>>16;

  //an empty buffer used to check if a message is an ack
  char test_buf[MAX_DATA_PER_PACKET] = {0};

  //if it is either a SYN-message or a SYN-ACK
  if((t_p->miptp_header & SEQ_NR_MASK) == 0){
    int i;
    for(i = 0; i<MAX_APP_CONNECTIONS; i++){
      //have to check if any apps are connected to the source/dest port
      if(incoming_pn == ep_ctrl->connected_apps[i].port_number){

        //check if it is a SYN-ACK
        if(memcmp(test_buf, t_p->data, MAX_DATA_PER_PACKET) == 0){
          if(ep_ctrl->connected_apps[i].ack_nr == -1){
            debug_print("Has received SYN-ACK\n", ep_ctrl->connected_apps[i].port_number);
            ep_ctrl->connected_apps[i].ack_nr = 0;
            rv = send_packages(ep_ctrl, i, ep_ctrl->connected_apps[i].ack_nr + 1, 10);
            if(rv == -1){
              return -1;
            }
            return 0;
          } else{
            return -3;
          }
        }

        //check if someone tries to connect to a port that allready is connected to someone
        if(ep_ctrl->connected_apps[i].ack_nr != -2){
          debug_print("Has rejected a new connection. The client has to wait for either timeout or until the server has received the whole file\n", ep_ctrl->connected_apps[i].port_number);
          return 0;
        }


        //if we get here, then it is a SYN-message that's going to be accepted
        uint16_t file_size = 0;
        memcpy(&file_size, t_p->data, 2);
        ep_ctrl->connected_apps[i].file_size = file_size;
        ep_ctrl->connected_apps[i].receiving_end = 1;
        ep_ctrl->connected_apps[i].package_sent = time(NULL);
        ep_ctrl->connected_apps[i].ack_nr = 0;
        ep_ctrl->connected_apps[i].destination_mip = mip_adr_to_sender;

        debug_print("Has received a setup-message from the sender side.\n", ep_ctrl->connected_apps[i].port_number);
        rv = send_ack(ep_ctrl, 0, ep_ctrl->connected_apps[i].port_number, ep_ctrl->connected_apps[i].destination_mip);
        if(rv == -1){
          return -1;
        }

        return 1;
      }
    }

    return 0;
  } else if(((t_p->miptp_header & SEQ_NR_MASK) == SEQ_NR_MASK) && (memcmp(t_p->data, test_buf, MAX_DATA_PER_PACKET) != 0)){
    //if the program gets here, it has received a teardown-message
    int i;
    for(i = 0; i<MAX_APP_CONNECTIONS; i++){
      if(incoming_pn == ep_ctrl->connected_apps[i].port_number){
        debug_print("Has received a teardown-message.\n", ep_ctrl->connected_apps[i].port_number);
        debug_print("Sending teardown-ACK\n", ep_ctrl->connected_apps[i].port_number);
        if(send_ack(ep_ctrl, SEQ_NR_MASK, ep_ctrl->connected_apps[i].port_number, ep_ctrl->connected_apps[i].destination_mip) == -1){
          return -1;
        }

        //resetting the variables on this app-connetion
        ep_ctrl->connected_apps[i].destination_mip = -1;
        ep_ctrl->connected_apps[i].ack_nr = -2;
        ep_ctrl->connected_apps[i].receiving_end = 0;
        ep_ctrl->connected_apps[i].file_size = 0;
        memset(ep_ctrl->connected_apps[i].packets, 0, sizeof((MAX_FILE_SIZE/MAX_DATA_PER_PACKET)+1));
        return 0;
      }
    }
    return 0;
  }


  //check if the package is an ack
  if(memcmp(test_buf, t_p->data, MAX_DATA_PER_PACKET) == 0){
    int i;
    for(i = 0; i<MAX_APP_CONNECTIONS; i++){
      if((ep_ctrl->connected_apps[i].port_number == incoming_pn)){
        //check if has received an ack that are higher than it has received earlier
        if((t_p->miptp_header & SEQ_NR_MASK) > (unsigned int)ep_ctrl->connected_apps[i].ack_nr){
          int num_new_elem_in_window = (t_p->miptp_header & SEQ_NR_MASK) - ep_ctrl->connected_apps[i].ack_nr;
          ep_ctrl->connected_apps[i].ack_nr = (t_p->miptp_header & SEQ_NR_MASK);

          //check if it is a teardown ack
          if((t_p->miptp_header & SEQ_NR_MASK) == SEQ_NR_MASK){
            debug_print("Has received ack on teardown-message\n", ep_ctrl->connected_apps[i].port_number);
            //sending message to file_server_client
            char* msg_to_client = "The file was successfully sent!";
            rv = send(ep_ctrl->connected_apps[i].fd, msg_to_client, strlen(msg_to_client), 0);
            if(rv == -1){
              perror("main(): epoll_event(): recv_packages(): send()");
              return -1;
            }
            return 0;
          }
          debug_print_ack_client(ep_ctrl, i);


          //check if it has sent all packages
          if(ep_ctrl->connected_apps[i].packets[ep_ctrl->connected_apps[i].ack_nr + 1].length == 0){
            if(send_teardown_msg(ep_ctrl, i) == -1){
              return -1;
            }
            ep_ctrl->connected_apps[i].ack_nr = 65534;
            return 0;
          }

          rv = send_packages(ep_ctrl, i, ep_ctrl->connected_apps[i].ack_nr + 12 - num_new_elem_in_window, num_new_elem_in_window);
          if(rv == -1){
            return -1;
          }

          return 0;

        } else{
          //if it has received an ack that it has received before
          return 0;
        }
      }

    }
  }

  //if the program gets here, then it is an package with payload
  int i;
  for(i = 0; i<MAX_APP_CONNECTIONS; i++){
    if((ep_ctrl->connected_apps[i].port_number == incoming_pn)){
      debug_print_recv_server(t_p->miptp_header & SEQ_NR_MASK, ep_ctrl->connected_apps[i].port_number);
      //check if it's the right package that has arrived
      if((t_p->miptp_header & SEQ_NR_MASK) != (unsigned int)(ep_ctrl->connected_apps[i].ack_nr + 1)){
        //it's a wrong package, and sending back an ack on the last correct package
        if(send_ack(ep_ctrl, ep_ctrl->connected_apps[i].ack_nr, ep_ctrl->connected_apps[i].port_number, ep_ctrl->connected_apps[i].destination_mip)){
          return -1;
        }
        return 0;
      }

      //if it is the first package (not set-up-message), then it has to send the file_length to server
      if((t_p->miptp_header & SEQ_NR_MASK) == 1){
        debug_print("Sending info of file to file server\n", ep_ctrl->connected_apps[i].port_number);
        rv = send(ep_ctrl->connected_apps[i].fd, &ep_ctrl->connected_apps[i].file_size, 2, 0);
        if(rv == -1){
          perror("main(): epoll_event(): recv_packages(): recv()");
          return -1;
        }
      }


      //sending the payload to server
      int padding = t_p->miptp_header>>30;
      int bytes_to_send = ((mip_payload_length-1)*4) - padding;
      if(debugmode == 1){
        printf("-- Port: %d --  Sending bytes to file server from package %d\n", ep_ctrl->connected_apps[i].port_number, ep_ctrl->connected_apps[i].ack_nr + 1);
      }
      rv = send(ep_ctrl->connected_apps[i].fd, t_p->data, bytes_to_send, 0);
      if(rv == -1){
        perror("main(): epoll_event(): recv_packages(): recv()");
        return -1;
      }

      //sending ack on the package
      ep_ctrl->connected_apps[i].ack_nr += 1;
      debug_print_send_ack(ep_ctrl, i);
      rv = send_ack(ep_ctrl, ep_ctrl->connected_apps[i].ack_nr, ep_ctrl->connected_apps[i].port_number, ep_ctrl->connected_apps[i].destination_mip);
      if(rv == -1){
        return -1;
      }
      return 0;
    }
  }
  return 0;
}

/*
  This function accepts a new application and adds it to the epoll

  Parameters:
    ep_ctrl: a pointer to the epoll_control struct that contains info of the epoll

  Return values:
    On success it returns the index the connection got in the table
    -1: on errors
    -2: if connected_apps is full
*/
int add_new_app(struct epoll_control_transport* ep_ctrl){
  //find a free index
  int index;
  for(index = 0; index<MAX_APP_CONNECTIONS; index++){
    if(ep_ctrl->connected_apps[index].fd == 0){
      break;
    }
  }
  if(index == MAX_APP_CONNECTIONS){
    fprintf(stderr, "It's not possible to add more to this daemon\n");
    return -2;
  }

  struct sockaddr_un sockaddr = {0};
  sockaddr.sun_family = AF_UNIX;
  socklen_t addrlen = sizeof(sockaddr);

  ep_ctrl->connected_apps[index].fd = accept(ep_ctrl->listen_sock, (struct sockaddr *)&sockaddr, &addrlen);
  if(ep_ctrl->connected_apps[index].fd == -1){
    perror("main(): epoll_event(): add_new_app(): accept():");
    return -1;
  }

  struct epoll_event ev = {0};
  ev.events = EPOLLIN;
  ev.data.fd = ep_ctrl->connected_apps[index].fd;
  if(epoll_ctl(ep_ctrl->epoll_fd, EPOLL_CTL_ADD, ep_ctrl->connected_apps[index].fd, &ev) == -1) {
    perror("main(): epoll_event(): add_new_app(): epoll_ctl()");
    return -1;
  }


  return index;
}


/*
  This function checks if there are packages which hasen't get any response
  (within the deadline "timeout"), and sends again if it has.

  Parameters:
    ep_ctrl: a pointer to the epoll_control struct that contains info of the epoll
    timeout_value: the timeout value which is sent in as a command line argument
      when starting the daemon

  Return values:
    On success it returns the shortest time until a new timeout happens, so that the program
    doesn't have to check unnecessary times. On errors it returns -1.
*/
int check_for_timeout(struct epoll_control_transport* ep_ctrl, int timeout_value){
  int temp_shortest = TIMEOUT_SYNACK;
  int i;
  for(i = 0; i<MAX_APP_CONNECTIONS; i++){
    //if no packages arrives after receiving the SYN-msg
    if(ep_ctrl->connected_apps[i].receiving_end == 1 && ((time(NULL) - ep_ctrl->connected_apps[i].package_sent) > TIMEOUT_SYNACK) && ep_ctrl->connected_apps[i].ack_nr == 0){
      ep_ctrl->connected_apps[i].destination_mip = -1;
      ep_ctrl->connected_apps[i].ack_nr = -2;
      ep_ctrl->connected_apps[i].receiving_end = 0;
      ep_ctrl->connected_apps[i].file_size = 0;
      debug_print("Has disconnected a connection, because no ACK on SYN-ACK arrived\n", ep_ctrl->connected_apps[i].port_number);
      continue;
    }

    //checks if it is an empty index in the array, or if the connected app is an server
    if(ep_ctrl->connected_apps[i].fd == 0 || ep_ctrl->connected_apps[i].receiving_end == 1 || ep_ctrl->connected_apps[i].ack_nr == -2){
      continue;
    }


    if((time(NULL) - ep_ctrl->connected_apps[i].package_sent) > timeout_value){

      //checks if it is the teardown-message that has to be sent again
      if(ep_ctrl->connected_apps[i].ack_nr == 65534){
        if(ep_ctrl->connected_apps[i].teardown_sent_counter == 3){
          debug_print("Has sent teardown-message 3 times, assumes that the ack from the server side got lost, and that they have closed the connection to this session\n", ep_ctrl->connected_apps[i].port_number);
          char* msg_to_client = "The file was successfully sent!";
          int rv = send(ep_ctrl->connected_apps[i].fd, msg_to_client, strlen(msg_to_client), 0);
          if(rv == -1){
            perror("main(): epoll_event(): recv_packages(): send()");
            return -1;
          }
          continue;
        }

        if(send_teardown_msg(ep_ctrl, i) == -1){
          return -1;
        }
        continue;
      } else if(ep_ctrl->connected_apps[i].ack_nr == -1){
        /*
          Checks if it's a timeout on the setup-message, and if it is it sends a
          message to the client that it not was able to connect. The reason that
          I close and not send again until the server answers is because it's possible that
          no one is listening on the port on the server side or someone allready has a
          connection to that port. And if so, it's not good to spam the server with requests
          that not is going to be accepted.
        */
        char* msg_to_client = "The program was unable to connect to the server\n";
        int rv = send(ep_ctrl->connected_apps[i].fd, msg_to_client, strlen(msg_to_client), 0);
        if(rv == -1){
          perror("main(): epoll_event(): recv_packages(): send()");
          printf("-1 paa send()\n");
          return -1;
        }
        continue;
      }

      //sends the window again, assumes all packages is lost
      int rv = send_packages(ep_ctrl, i, ep_ctrl->connected_apps[i].ack_nr + 1, 10);
      if(rv == -1){
        return -1;
      }
    }

    //updates the temp_shortest
    int temp_time = timeout_value - (time(NULL) - ep_ctrl->connected_apps[i].package_sent);
    if(temp_time < temp_shortest){
      temp_shortest = temp_time;
    }

  }

  if(temp_shortest < 1){
    return 1;
  }
  return temp_shortest;
}

/*
  This function frees memory which is used to store packages on the heap.

  Parameters:
    ep_ctrl: a pointer to the epoll_control struct that contains info of the epoll
    index: index in the connected_apps-array where the info of this connection and
     packages is stored

*/
void free_packages(struct epoll_control_transport* ep_ctrl, int index){
  int k;
  for(k = 0; k<(MAX_FILE_SIZE/MAX_DATA_PER_PACKET)+1 && ep_ctrl->connected_apps[index].packets[k].length != 0; k++){
    free(ep_ctrl->connected_apps[index].packets[k].transport_packet);
  }
}

/*
  This functions closes all sockets to connected application.

  Parameters:
    ep_ctrl: a pointer to a epoll_control struct which contains info of the epoll
*/
void close_appsockets(struct epoll_control_transport* ep_ctrl){
  int i;
  for(i = 0; i<MAX_APP_CONNECTIONS; i++){
    if(ep_ctrl->connected_apps[i].fd > 0){
      close(ep_ctrl->connected_apps[i].fd);
    }
  }
}

/*
  This function has the responsibility to handle all types of events on the sockets,
  and call all necessary functions. If it is an event from a client, it also
  splits the file into packages.

  Parameters:
    ep_ctrl: a pointer to a epoll_control struct which contains info of the epoll
    n: the index in the events array in the ep_ctrl, for getting the right epoll_event struct

  Return values:
    1: A new app has connected to this daemon
    0: On general success
    -1: On errors
    -2: if the mip daemon has disconnected
*/
int epoll_event(struct epoll_control_transport* ep_ctrl, int n){
  if(ep_ctrl->events[n].data.fd == ep_ctrl->listen_sock){
    int rv = add_new_app(ep_ctrl);
    if(rv == -1){
      return -1;
    } else if(rv == -2){
      return 0;
    } else{
      //receives info of the app that has connected
      struct app_info* app_inf = malloc(sizeof(struct app_info));
      int rv2 = recv(ep_ctrl->connected_apps[rv].fd, app_inf, sizeof(struct app_info), 0);
      if(rv2 == -1){
        perror("main(): epoll_event(): recv()");
        free(app_inf);
        return -1;
      }
      ep_ctrl->connected_apps[rv].port_number = app_inf->port_number;
      ep_ctrl->connected_apps[rv].destination_mip = app_inf->dest_mip;
      ep_ctrl->connected_apps[rv].ack_nr = -2;
      debug_print("A new application has connected to transport daemon!\n", ep_ctrl->connected_apps[rv].port_number);
      free(app_inf);
      return 1;
    }
  }

  //check if it is from some connected apps
  int i;
  for(i = 0; i<MAX_APP_CONNECTIONS; i++){
    if(ep_ctrl->events[n].data.fd == ep_ctrl->connected_apps[i].fd){
      //receives the file
      struct file_struct f_struct = {0};
      int rv = recv(ep_ctrl->connected_apps[i].fd, &f_struct, sizeof(struct file_struct), 0);
      if(rv == -1){
        perror("main(): epoll_event(): recv()");
        return -1;
      } else if(rv == 0){
        printf("An application has performed an orderly shutdown\n");
        if(epoll_ctl(ep_ctrl->epoll_fd, EPOLL_CTL_DEL, ep_ctrl->connected_apps[i].fd, &ep_ctrl->events[n]) == -1){
          perror("main(): epoll_event(): epoll_ctl():");
          return -1;
        }
        free_packages(ep_ctrl, i);
        close(ep_ctrl->connected_apps[i].fd);
        memset(&ep_ctrl->connected_apps[i], 0, sizeof(struct connected_apps_struct));
        return 0;
      }


      //adds the startup-message
      ep_ctrl->connected_apps[i].packets[0].transport_packet = malloc(sizeof(struct transport_packet) + 2);
      ep_ctrl->connected_apps[i].packets[0].transport_packet->miptp_header = (2<<30) | (ep_ctrl->connected_apps[i].port_number<<16);
      memcpy(ep_ctrl->connected_apps[i].packets[0].transport_packet->data, &f_struct.file_size, 2);
      ep_ctrl->connected_apps[i].packets[0].length = sizeof(struct transport_packet) + 2;
      ep_ctrl->connected_apps[i].file_size = f_struct.file_size;

      //here the file are split into packages and stored with the info of the app connection
      int temp_size = f_struct.file_size;
      char* buf_pointer = f_struct.file;
      int k;
      for(k = 1; k<(MAX_FILE_SIZE/MAX_DATA_PER_PACKET)+2 && !(temp_size < 1); k++){
        if(temp_size >= MAX_DATA_PER_PACKET){
          ep_ctrl->connected_apps[i].packets[k].transport_packet = malloc(sizeof(struct transport_packet) + MAX_DATA_PER_PACKET);
          ep_ctrl->connected_apps[i].packets[k].transport_packet->miptp_header = (ep_ctrl->connected_apps[i].port_number<<16) | k;
          memcpy(ep_ctrl->connected_apps[i].packets[k].transport_packet->data, buf_pointer, MAX_DATA_PER_PACKET);
          ep_ctrl->connected_apps[i].packets[k].length = sizeof(struct transport_packet) + MAX_DATA_PER_PACKET;
          temp_size = temp_size - MAX_DATA_PER_PACKET;
          buf_pointer = buf_pointer + MAX_DATA_PER_PACKET;
        } else{
          //the last package
          unsigned int pl = 4-(temp_size%4);
          ep_ctrl->connected_apps[i].packets[k].transport_packet = malloc(sizeof(struct transport_packet) + temp_size);
          ep_ctrl->connected_apps[i].packets[k].transport_packet->miptp_header = (pl<<30) | (ep_ctrl->connected_apps[i].port_number<<16) | k;
          memcpy(ep_ctrl->connected_apps[i].packets[k].transport_packet->data, buf_pointer, temp_size);
          ep_ctrl->connected_apps[i].packets[k].length = sizeof(struct transport_packet) + temp_size;
          break;
        }
      }


      rv = establish_connection(ep_ctrl, i);
      return 0;
    }
  }

  //if it receives a package from mip daemon
  if(ep_ctrl->events[n].data.fd == ep_ctrl->mip_daemon_fd){
    int rv = recv_packages(ep_ctrl);
    if(rv == -2){
      close_appsockets(ep_ctrl);
      return -2;
    }
  }

  return 0;
}


/*
  This function set the listen_sock variable in ep_ctrl, and makes it possible for applications
  to connect to the daemon in ep_ctrl.

  Parameters:
    ep_ctrl: a pointer to a epoll_control struct which contains info of the epoll
    sockpath: the application socketpath that is typed in as command line argument

  Return values:
    0 on success and -1 on errors.
*/
int set_listen_sock(struct epoll_control_transport* ep_ctrl, char* sockpath){

  ep_ctrl->listen_sock = socket(AF_UNIX, SOCK_SEQPACKET, 0);
  if(ep_ctrl->listen_sock == -1){
    perror("main(): set_listen_sock():");
    return -1;
  }


  struct sockaddr_un sockaddr = {0};
  sockaddr.sun_family = AF_UNIX;
  strncpy(sockaddr.sun_path, sockpath, sizeof(sockaddr.sun_path)-1);

  if(bind(ep_ctrl->listen_sock, (struct sockaddr *)&sockaddr, sizeof(sockaddr)) == -1) {
    perror("main(): set_listen_sock(): bind()");
    return -1;
  }


  if(listen(ep_ctrl->listen_sock, 10) == -1){
    perror("main(): listen()");
    return -1;
  }

  struct epoll_event ev = {0};
  ev.events = EPOLLIN;
  ev.data.fd = ep_ctrl->listen_sock;
  if(epoll_ctl(ep_ctrl->epoll_fd, EPOLL_CTL_ADD, ep_ctrl->listen_sock, &ev) == -1) {
    perror("main(): set_listen_sock(): epoll_ctl: listen_sock");
    return -1;
  }

  return 0;
}

/*
  This function checks if the input parameters is typed correct, and prints
  usage-message if it don't.

  Parameters:
    The same as for main - argc is number of arguments and argv is the values

  Return values
    It returns 1 if the debug flag is typed, and 0 if it don't.
*/
int check_input_parameters(int argc, char* argv[]){
  if(argc == 5){
    if(strcmp(argv[1], "-d") == 0){
      return 1;
    } else{
      printf("Usage: %s [-d] <transport daemon socketpath> <application socketpath> <timeout>\n", argv[0]);
      exit(EXIT_SUCCESS);
    }
  } else if(argc != 4){
    printf("Usage: %s [-d] <transport daemon socketpath> <application socketpath> <timeout>\n", argv[0]);
    exit(EXIT_SUCCESS);
  }
  return 0;
}

int main(int argc, char* argv[]){
  int debug = check_input_parameters(argc, argv);
  struct epoll_control_transport ep_ctrl = {0};
  char* socketpath = {0};
  char* socketpath_apps = {0};
  int timeout = 0;
  if(debug == 1){
    debugmode = 1;
    socketpath = argv[2];
    socketpath_apps = argv[3];
    timeout = atoi(argv[4]);
  } else{
    socketpath = argv[1];
    socketpath_apps = argv[2];
    timeout = atoi(argv[3]);
  }

  ep_ctrl.mip_daemon_fd = socket(AF_UNIX, SOCK_SEQPACKET, 0);
  if(ep_ctrl.mip_daemon_fd == -1){
    perror("main(): socket()");
    exit(EXIT_FAILURE);
  }

  ep_ctrl.epoll_fd = epoll_create(99);
  if(ep_ctrl.epoll_fd == -1) {
    perror("main(): epoll_create()");
    close(ep_ctrl.mip_daemon_fd);
    exit(EXIT_FAILURE);
  }

  struct sockaddr_un sockaddr;
  sockaddr.sun_family = AF_UNIX;
  strncpy(sockaddr.sun_path, socketpath, sizeof(sockaddr.sun_path)-1);

  //connect to mip daemon
  if(connect(ep_ctrl.mip_daemon_fd, (struct sockaddr*)&sockaddr, sizeof(sockaddr)) == -1){
    perror("main(): connect():");
    close(ep_ctrl.mip_daemon_fd);
    exit(EXIT_FAILURE);
  }

  struct epoll_event ev = {0};
  ev.events = EPOLLIN;
  ev.data.fd = ep_ctrl.mip_daemon_fd;
  if (epoll_ctl(ep_ctrl.epoll_fd, EPOLL_CTL_ADD, ep_ctrl.mip_daemon_fd, &ev) == -1) {
    perror("epoll_ctl:");
    close(ep_ctrl.mip_daemon_fd);
    exit(EXIT_FAILURE);
  }

  //sets up listen socket for apps
  if(set_listen_sock(&ep_ctrl, socketpath_apps) == -1){
    return EXIT_FAILURE;
  }

  int epoll_wait_timeout_value = 1000;
  int ret = 0;
  /*
    This loop stops and blocks in epoll_wait until an event is ready for I/O,
    or until one of the connections reaches a timeout.
  */
  while(1){
    int nr_fd_ready = epoll_wait(ep_ctrl.epoll_fd, ep_ctrl.events, MAX_EVENTS, epoll_wait_timeout_value);
    if(nr_fd_ready == -1){
      perror("main(): epoll_wait()");
      close_appsockets(&ep_ctrl);
      exit(EXIT_FAILURE);
    } else if(nr_fd_ready == 0){
      ret = check_for_timeout(&ep_ctrl, timeout);
      if(ret == -1){
        close_appsockets(&ep_ctrl);
        return EXIT_FAILURE;
      } else{
        epoll_wait_timeout_value = ret * 1000;
      }
      continue;
    }
    int n;
    for (n = 0; n<nr_fd_ready; n++){
      int rv = epoll_event(&ep_ctrl, n);
      if(rv == -1){
        close_appsockets(&ep_ctrl);
        return EXIT_FAILURE;
      } else if(rv == -2){
        close_appsockets(&ep_ctrl);
        return EXIT_SUCCESS;
      }
    }
    ret = check_for_timeout(&ep_ctrl, timeout);
    if(ret == -1){
      close_appsockets(&ep_ctrl);
      return EXIT_FAILURE;
    } else{
      epoll_wait_timeout_value = ret * 1000;
    }
  }

}
