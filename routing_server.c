#define _GNU_SOURCE
#include <sys/epoll.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/types.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <limits.h>
#include <time.h>
#include "mip_header.h"

/* the time limit for removing inactive neighbor hosts */
#define TIMELIMIT_NEIGHBORS 60

/* array of the mip addresses which belongs to this host */
int mipadr_on_this_host[MAX_INTERFACES] = {0};
/* a "boolean" which has the value 1 when it shall send out initialization info */
int start_routing_process = 0;

/* distance table */
struct distance_table_struct distance_table[MAX_TABLE_SIZE];
/* routing table */
struct dest_outgoing_cost routing_table[MAX_TABLE_SIZE];

int update_routing_table();
int send_routing_info(struct epoll_control_router* ep_ctrl);

/*
  This function finds the next hop, so the mip_daemon knows which neighbor it will
  send the frame to.

  Parameters:
    dest_mip: The mip address of the destination.

  Return values:
    It returns the address to the next host. If the routing table dosen't have
    the destination address, it returns -1.
*/
int get_outgoing_mipaddr(int dest_mip){
  int i;
  for(i = 0; i<MAX_TABLE_SIZE; i++){
    int j;
    for(j = 0; j<MAX_INTERFACES; j++){
      if(routing_table[i].dest_mip[j] == dest_mip){
        return routing_table[i].outgoing_mip;
      }
    }
  }
  return -1;
}

/*
  This function removes a host in the distance table and routing table, if the connection
  to the hos tis closed. It changes the global variables routing_table and distance_table.

  Parameters:
    index: index to the distance table where the host that is going to be removed is placed.
*/
void remove_host(int index){
  int mipadr_neighbor = get_outgoing_mipaddr(distance_table[index].dest_mip[0]);
  memset(&distance_table[index], 0, sizeof(struct distance_table_struct));

  int i, j;
  for(i = 0; i<MAX_TABLE_SIZE; i++){
    for(j = 0; j<MAX_INTERFACES; j++){
      if(distance_table[i].via_and_cost[j].outgoing_mip == mipadr_neighbor){
        distance_table[i].via_and_cost[j].outgoing_mip = 0;
        distance_table[i].via_and_cost[j].cost = 0;
      }
    }
  }

  for(i = 0; i<MAX_TABLE_SIZE; i++){
    if(routing_table[i].outgoing_mip == mipadr_neighbor){
      memset(&routing_table[i], 0, sizeof(struct dest_outgoing_cost));
    }
  }

  update_routing_table();
}


/*
  This function checks if any neighbors timelimit has expired. If it has, the function
  call the necessary function to remove host and send out the new routing table.

  Parameters:
    ep_ctrl: a pointer to a epoll_control struct which contains info of the epoll
*/
void check_timeout_neighbors(struct epoll_control_router* ep_ctrl){
  int i;
  for(i = 0; i<MAX_TABLE_SIZE; i++){
    if(distance_table[i].neighbor == 1){
      if((time(NULL)-distance_table[i].last_received_from_host) > TIMELIMIT_NEIGHBORS){
        remove_host(i);
        send_routing_info(ep_ctrl);
      }
    }
  }
}

/*
  This function updates the routing table by picking the cheapest paths from
  the distance table on each destination. Thus, the function can change the state
  of the global variable routing table.

  Return values:
    It returns 1 if it has updated the routing table, and 0 if the routing table
    was not updated.

*/
int update_routing_table(){
  int i, j, k;
  int have_updated_table = 0;
  //looping through the distance table
  for(i = 0; i<MAX_TABLE_SIZE; i++){
    if(distance_table[i].dest_mip[0] == 0){
      continue;
    }
    int temp_dest_distance = distance_table[i].dest_mip[0];
    int new_destination = 1;

    //find index to shortest path in distance_table
    int index_to_cheapest_path = -1;
    int temp_shortest = INT_MAX;
    for(j = 0; j<MAX_INTERFACES; j++){
      if((distance_table[i].via_and_cost[j].cost < temp_shortest) && (distance_table[i].via_and_cost[j].outgoing_mip != 0)){
        temp_shortest = distance_table[i].via_and_cost[j].cost;
        index_to_cheapest_path = j;
      }
    }

    //looping through the routing table
    for(j = 0; j<MAX_TABLE_SIZE; j++){
      for(k = 0; k<MAX_INTERFACES; k++){
        int temp_dest_routing = routing_table[j].dest_mip[k];
        if(temp_dest_distance == temp_dest_routing){
          //set the shortest in distance_table to be the one in routing_table
          if((routing_table[j].cost != distance_table[i].via_and_cost[index_to_cheapest_path].cost) || (routing_table[j].outgoing_mip != distance_table[i].via_and_cost[index_to_cheapest_path].outgoing_mip)){
            have_updated_table = 1;
          }
          routing_table[j].cost = distance_table[i].via_and_cost[index_to_cheapest_path].cost;
          routing_table[j].outgoing_mip = distance_table[i].via_and_cost[index_to_cheapest_path].outgoing_mip;
          new_destination = 0;
        }
      }
    }
    if(new_destination == 0){
      continue;
    }

    /* if the program gets here the routing table dosen't have the dest mip
        in routing table, and it will be added */
    have_updated_table = 1;
    for(j = 0; j<MAX_TABLE_SIZE; j++){
      if(routing_table[j].outgoing_mip == 0){
        memcpy(&routing_table[j].dest_mip[0], &distance_table[i].dest_mip[0], sizeof(int)*MAX_INTERFACES);
        routing_table[j].outgoing_mip = distance_table[i].via_and_cost[index_to_cheapest_path].outgoing_mip;
        routing_table[j].cost = distance_table[i].via_and_cost[index_to_cheapest_path].cost;
        break;
      }
    }
  }

  return have_updated_table;
}


/*
  This function sends this host's routing table on all of its interfaces.

  Parameters:
    ep_ctrl: a pointer to a epoll_control struct which contains info of the epoll

  Return values:
    It returns 0 on success and -1 on errors.
*/
int send_routing_info(struct epoll_control_router* ep_ctrl){
  int routing_table_size = sizeof(struct dest_outgoing_cost)*MAX_TABLE_SIZE;
  int i;
  //send the routing info on all interfaces
  for(i = 0; (i<MAX_INTERFACES) && (mipadr_on_this_host[i] != 0); i++){
    struct network_datagram* datagram = malloc(sizeof(struct network_datagram)+routing_table_size);
    if(datagram == NULL){
      return -1;
    }
    size_t length = 0;
    if((sizeof(struct network_datagram)+routing_table_size)%4 == 0){
      length = (sizeof(struct network_datagram)+routing_table_size)/4;
    } else{
      length = ((sizeof(struct network_datagram)+routing_table_size) + (4 - ((sizeof(struct network_datagram)+routing_table_size)%4)))/4;
    }

    uint32_t network_header = (15 | (length<<4) | (mipadr_on_this_host[i]<<13) | (2<<29));
    datagram->network_header = network_header;

    memcpy(datagram->msg, routing_table, routing_table_size);

    struct iovec iov[2];

    iov[0].iov_base = datagram;
    iov[0].iov_len = sizeof(struct network_datagram)+routing_table_size;

    uint32_t mipaddr_msg_is_going_to_be_sent_from = mipadr_on_this_host[i];
    iov[1].iov_base = &mipaddr_msg_is_going_to_be_sent_from;
    iov[1].iov_len = 4;


    struct msghdr message = {0};
    message.msg_iov = iov;
    message.msg_iovlen = 2;

    if(sendmsg(ep_ctrl->routing_fd, &message, 0) == -1){
      perror("main(): send_routing_info(): sendmsg()");
      free(datagram);
      return -1;
    }
    free(datagram);

  }
  return 0;
}

/*
  This table returns an index to a free space in the distance table.

  Return values:
    On success it returns the index to the free space, if the table is full
    the function returns -1.
*/
int get_free_distance_table_index(){
  int i, j;
  for(i = 0; i<MAX_TABLE_SIZE; i++){
    for(j = 0; j<MAX_INTERFACES; j++){
      if(distance_table[i].via_and_cost[j].outgoing_mip != 0){
        break;
      }
      if(j == MAX_INTERFACES-1){
        return i;
      }
    }
  }
  return -1;
}

/*
  This function reads from the incoming routing table, and it checks if something
  can be updated. The function changes the state of the global variable "distance_table",
  if some updates is necessary. The function has the responsibility to update both the
  distance table and routing table.

  Parameters:
    datagram: a struct that contains the mip-header and the incoming routing table
    ep_ctrl: a pointer to a epoll_control struct which contains info of the epoll

  Return values:
    It returns 0 on success and -1 on errors.
*/
int update_tables(struct network_datagram* datagram, struct epoll_control_router* ep_ctrl){
  //transfers the incoming routing table from datagram->msg to a local array of structs
  int incoming_has_me = 0;
  int source_mip_mask = 2088960;
  int senders_mipaddr = (datagram->network_header & source_mip_mask)>>13;
  struct dest_outgoing_cost routing_table_incoming[MAX_TABLE_SIZE];
  memset(routing_table_incoming, 0, sizeof(routing_table_incoming));

  int i;
  char* temp_start_pointer = datagram->msg;
  for(i = 0; i<MAX_TABLE_SIZE; i++){
    struct dest_outgoing_cost temp_doc_struct;
    memcpy(&temp_doc_struct, temp_start_pointer, sizeof(struct dest_outgoing_cost));
    memcpy(&routing_table_incoming[i], temp_start_pointer, sizeof(struct dest_outgoing_cost));
    temp_start_pointer = temp_start_pointer + sizeof(struct dest_outgoing_cost);
  }



  //checks if there are hosts/dest mip addrs that I don't have in current distance table
  for(i = 0; i<MAX_TABLE_SIZE; i++){
    int temp_mip = routing_table_incoming[i].dest_mip[0];
    if(temp_mip == 0){
      continue;
    } else if(temp_mip == mipadr_on_this_host[0]){
      incoming_has_me = 1;
    }
    int update = 1;
    int j;
    for(j = 0; j<MAX_TABLE_SIZE; j++){
      int k;
      for(k = 0; k<MAX_INTERFACES; k++){
        if(distance_table[j].dest_mip[k] == temp_mip){
          update = 0;
          int have_via_mipaddr_from_before = 0;

          //check if I have the destination via the host which sent this routing table,
          //and if so, update the cost
          int m;
          for(m = 0; m<MAX_INTERFACES; m++){
            if(distance_table[j].via_and_cost[m].outgoing_mip == senders_mipaddr){
              if(distance_table[j].via_and_cost[m].cost != routing_table_incoming[i].cost + 1){
                //to prevent "count to infinity" (poisoned reverse/split horizon)
                int going_to_send_back_to_me = 0;
                int n;
                for(n = 0; n<MAX_INTERFACES; n++){
                  if(routing_table_incoming[i].outgoing_mip == mipadr_on_this_host[n]){
                    going_to_send_back_to_me = 1;
                    break;
                  }
                }

                if(going_to_send_back_to_me == 1){
                  distance_table[j].via_and_cost[m].cost = INT_MAX;
                } else{
                  distance_table[j].via_and_cost[m].cost = routing_table_incoming[i].cost + 1;
                }

              }
              have_via_mipaddr_from_before = 1;
              break;
            }
          }

          //dosen't have via the incoming host, thus we have to add it to the dis. table
          if(have_via_mipaddr_from_before == 0){
            for(m = 0; m<MAX_INTERFACES; m++){
              if(distance_table[j].via_and_cost[m].outgoing_mip == 0){
                distance_table[j].via_and_cost[m].outgoing_mip = senders_mipaddr;
                //to prevent "count to infinity" (poisoned reverse/split horizon)
                int going_to_send_back_to_me = 0;
                int n;
                for(n = 0; n<MAX_INTERFACES; n++){
                  if(routing_table_incoming[i].outgoing_mip == mipadr_on_this_host[n]){
                    going_to_send_back_to_me = 1;
                    break;
                  }
                }

                if(going_to_send_back_to_me == 1){
                  distance_table[j].via_and_cost[m].cost = INT_MAX;
                } else{
                  distance_table[j].via_and_cost[m].cost = routing_table_incoming[i].cost + 1;
                }
                break;
              }
            }
          }

          break;
        }
      }
      if(update == 0){
        break;
      }
    }
    if(update == 0){
      continue;
    }

    //if the program get's here, the distance table dosn't have temp_mip in the routing
    //table and it must be added
    int free_index = get_free_distance_table_index();

    int going_to_send_back_to_me = 0;
    int r;
    for(r = 0; r<MAX_INTERFACES; r++){
      if(routing_table_incoming[i].outgoing_mip == mipadr_on_this_host[r]){
        going_to_send_back_to_me = 1;
        break;
      }
    }

    if(going_to_send_back_to_me == 0){
      memcpy(&distance_table[free_index].dest_mip[0], &routing_table_incoming[i].dest_mip[0], sizeof(int)*MAX_INTERFACES);
      distance_table[free_index].via_and_cost[0].cost = routing_table_incoming[i].cost + 1;
      distance_table[free_index].via_and_cost[0].outgoing_mip = senders_mipaddr;
    }



  }

  //updates last_received_from_host
  int break_loop = 0;
  for(i = 0; i<MAX_TABLE_SIZE; i++){
    int l;
    for(l = 0; l<MAX_INTERFACES; l++){
      if(distance_table[i].dest_mip[l] == senders_mipaddr){
        distance_table[i].neighbor = 1;
        distance_table[i].last_received_from_host = time(NULL);
        break_loop = 1;
      }
      if(break_loop == 1){
        break;
      }
    }
    if(break_loop == 1){
      break;
    }
  }



  /*
    Looping the distance_table and look at cases where the distance table sends
    via "senders_mipaddr", and we have to check if "senders_mipaddr" still can reach
    these destinations. If they can't, then both routing table (if it's the cheapest path)
    and distance table have to delete the paths.
  */
  int j;
  for(i = 0; i<MAX_TABLE_SIZE; i++){
    for(j = 0; j<MAX_INTERFACES; j++){
      if(distance_table[i].via_and_cost[j].outgoing_mip == senders_mipaddr){
        //check if "senders_mipaddr" still can reach the destination
        int k, l, m;
        int delete = 1;
        int break_loop = 0;
        //looping through routing_table_incoming
        for(k = 0; k<MAX_TABLE_SIZE; k++){
          for(l = 0; l<MAX_INTERFACES; l++){
            //looper through all dest_mips i distance_table
            for(m = 0; m<MAX_INTERFACES; m++){
              if((routing_table_incoming[k].dest_mip[l] == distance_table[i].dest_mip[m]) && distance_table[i].dest_mip[m] != 0){
                //updates, f. ex a host has lost a mip addr
                memcpy(&distance_table[i].dest_mip[0], &routing_table_incoming[k].dest_mip[0], sizeof(int)*MAX_INTERFACES);
                break_loop = 1;
                delete = 0;
                break;
              }
            }
            if(break_loop == 1){
              break;
            }
          }
          if(break_loop == 1){
            break;
          }
        }
        if(delete == 1){
          /* "senders_mipaddr" can't reach this dest_mip no more, and therefore it
              has to be deleted */
          distance_table[i].via_and_cost[j].outgoing_mip = 0;
          distance_table[i].via_and_cost[j].cost = 0;
          int break_loop = 0;
          for(k = 0; k<MAX_TABLE_SIZE; k++){
            for(l = 0; l<MAX_INTERFACES; l++){
              for(m = 0; m<MAX_INTERFACES; m++){
                if((routing_table[k].dest_mip[l] == distance_table[i].dest_mip[m]) && distance_table[i].dest_mip[m] != 0){
                  memset(&routing_table[k], 0, sizeof(struct dest_outgoing_cost));
                  break_loop = 1;
                  break;
                }
              }
              if(break_loop == 1){
                break;
              }
            }
            if(break_loop == 1){
              break;
            }
          }

          //if all the via_and_cost structs are 0, then delete "dest_mip" too
          int delete_dest = 1;
          for(k = 0; k<MAX_INTERFACES; k++){
            if(distance_table[i].via_and_cost[k].outgoing_mip != 0 && distance_table[i].via_and_cost[k].cost != INT_MAX){
              delete_dest = 0;
            }
          }
          if(delete_dest == 1){
            memset(&distance_table[i], 0, sizeof(struct distance_table_struct));
          }
        }
      }
    }
  }



  int rv = update_routing_table();
  if(rv == 1 || incoming_has_me == 0){
    send_routing_info(ep_ctrl);
  }


  return 0;
}

/*
  This function adds an unix socket to the epoll.

  Parameters:
    ep_ctrl: a pointer to a epoll_control struct
    fd: the socket filedescriptor to the socket which is going to be added

  Return values:
    0: on success
    -1: on error
*/
int epoll_add(struct epoll_control_router* ep_ctrl, int fd){
  struct epoll_event ev = {0};
  ev.events = EPOLLIN | EPOLLET;
  ev.data.fd = fd;
  if(epoll_ctl(ep_ctrl->epoll_fd, EPOLL_CTL_ADD, fd, &ev) == -1){
    perror("main(): epoll_event(): epoll_add(): epoll_ctl()");
    return -1;
  }
  return 0;
}

/*
  This function is called when a filedescriptor is ready for I/O.
  It distinguish between a forwarding socket and a routing socket, and reads and writes
  from and to them. This function has the responsibility for checking what caused
  the requested I/O, and call the functions to handle the requests.
  This function can change the global variable "distance_table"

  Parameters:
    ep_ctrl: a pointer to a epoll_control struct which contains info of the epoll
    n: the index in the events array in the ep_ctrl, for getting the right epoll_event struct

  Return values:
    -1: on errors
    0: on success

*/
int epoll_event(struct epoll_control_router* ep_ctrl, int n){
  int con_sock;
  struct sockaddr_un sockaddr = {0};
  sockaddr.sun_family = AF_UNIX;
  socklen_t addrlen = sizeof(sockaddr);

  if(ep_ctrl->events[n].data.fd == ep_ctrl->accept_routing_fd){
    //add routing_fd
    con_sock = accept4(ep_ctrl->accept_routing_fd, (struct sockaddr *)&sockaddr, &addrlen, SOCK_NONBLOCK);
    if(con_sock == -1){
      perror("main(): epoll_event(): accept4()");
      return -1;
    }
    ep_ctrl->routing_fd = con_sock;
    if(epoll_add(ep_ctrl, con_sock) == -1){
      return -1;
    }
    return 0;
  } else if(ep_ctrl->events[n].data.fd == ep_ctrl->accept_forwarding_fd){
    //add forwarding_fd
    con_sock = accept4(ep_ctrl->accept_forwarding_fd, (struct sockaddr *)&sockaddr, &addrlen, SOCK_NONBLOCK);
    if(con_sock == -1){
      perror("main(): epoll_event(): accept4():");
      return -1;
    }
    ep_ctrl->forwarding_fd = con_sock;
    if(epoll_add(ep_ctrl, con_sock) == -1){
      return -1;
    }

    return 0;
  } else if(ep_ctrl->events[n].data.fd == ep_ctrl->forwarding_fd){
    /*
      If the program get's here, the daemon has asked routing_server which mip_addr
      is the next hop.
    */
    int mip_addr = 0;

    struct iovec iov[2];

    iov[0].iov_base = &mip_addr;
    iov[0].iov_len = sizeof(mip_addr);

    iov[1].iov_base = mipadr_on_this_host;
    iov[1].iov_len = sizeof(mipadr_on_this_host);

    struct msghdr message = {0};
    message.msg_iov = iov;
    message.msg_iovlen = 2;

    int rv = recvmsg(ep_ctrl->forwarding_fd, &message, 0);

    if(rv == -1){
      perror("main(): epoll_event(): recvmsg()");
      return -1;
    } else if(rv == 0){
      printf("Forwarding socket has performed an orderly shutdown\n");
      if(epoll_ctl(ep_ctrl->epoll_fd, EPOLL_CTL_DEL, ep_ctrl->forwarding_fd, &ep_ctrl->events[n]) == -1){
        perror("main(): epoll_event(): epoll_ctl():");
        return -1;
      }
      return 0;
    }
    if(mip_addr == -1){
      //daemon has sent info of which mip addrs this host has
      //initializing routing/distance table
      distance_table[0].via_and_cost[0].outgoing_mip = -1;
      distance_table[0].via_and_cost[0].cost = 0;
      int i;
      for(i = 0; i<MAX_INTERFACES; i++){
        distance_table[0].dest_mip[i] = mipadr_on_this_host[i];
      }

      update_routing_table();
      start_routing_process = 1;
    } else{
      int next_mip = get_outgoing_mipaddr(mip_addr);
      if(next_mip == -1){
        fprintf(stderr, "Error: Couldn't find mip address in routing table\n");
        mip_addr = next_mip;
        return 0;
      }
      mip_addr = next_mip;
      if(sendmsg(ep_ctrl->forwarding_fd, &message, 0) == -1){
        perror("main(): epoll_event(): sendmsg()");
        return -1;
      }
    }
    return 0;
  } else if(ep_ctrl->events[n].data.fd == ep_ctrl->routing_fd){
    /*
      If the program get's here, a neighbor host has sent his routing table
    */
    struct iovec iov[1];

    struct network_datagram* datagram = malloc(sizeof(struct network_datagram) + sizeof(struct dest_outgoing_cost)*MAX_TABLE_SIZE);

    iov[0].iov_base = datagram;
    iov[0].iov_len = sizeof(struct network_datagram)+(sizeof(struct dest_outgoing_cost)*MAX_TABLE_SIZE);

    struct msghdr message = {0};
    message.msg_iov = iov;
    message.msg_iovlen = 1;

    int rv = recvmsg(ep_ctrl->routing_fd, &message, 0);
    if(rv == -1){
      perror("main(): epoll_event(): check_eth_frame(): sendmsg()");
      free(datagram);
      return -1;
    } else if(rv == 0){
      printf("Routing socket has performed an orderly shutdown\n");

      if(epoll_ctl(ep_ctrl->epoll_fd, EPOLL_CTL_DEL, ep_ctrl->routing_fd, &ep_ctrl->events[n]) == -1){
        perror("main(): epoll_event(): epoll_ctl():");
        return -1;
      }
      free(datagram);
      return 0;
    }


    update_tables(datagram, ep_ctrl);

    free(datagram);
  } else{
    fprintf(stderr, "Error in epoll_event: Wrong fd\n");
    return -1;
  }
  return 0;
}

int main(int argc, char* argv[]){
  if(argc != 3){
    printf("Usage: %s <routing socketpath> <forwarding socketpath>\n", argv[0]);
    return EXIT_SUCCESS;
  }

  struct epoll_control_router ep_ctrl = {0};
  char* routing_socketpath = argv[1];
  char* forwarding_socketpath = argv[2];

  ep_ctrl.epoll_fd = epoll_create(99);
  if(ep_ctrl.epoll_fd == -1) {
    perror("main(): epoll_create()");
    exit(EXIT_FAILURE);
  }

  int sd = socket(AF_UNIX, SOCK_SEQPACKET, 0);
  if(sd == -1){
    perror("main(): socket()");
    exit(EXIT_FAILURE);
  }

  struct sockaddr_un sockaddr_routing;
  sockaddr_routing.sun_family = AF_UNIX;
  strncpy(sockaddr_routing.sun_path, routing_socketpath, sizeof(sockaddr_routing.sun_path)-1);

  if(bind(sd, (struct sockaddr *)&sockaddr_routing, sizeof(sockaddr_routing)) == -1){
    perror("main(): bind()");
    exit(EXIT_FAILURE);
  }

  if(listen(sd, 1) == -1){
    perror("main(): listen()");
    exit(EXIT_FAILURE);
  }

  ep_ctrl.accept_routing_fd = sd;

  struct epoll_event ev = {0};
  ev.events = EPOLLIN;
  ev.data.fd = sd;
  if (epoll_ctl(ep_ctrl.epoll_fd, EPOLL_CTL_ADD, ep_ctrl.accept_routing_fd, &ev) == -1) {
    perror("epoll_ctl: listen_sock");
    close(sd);
    exit(EXIT_FAILURE);
  }

  sd = socket(AF_UNIX, SOCK_SEQPACKET, 0);
  if(sd == -1){
    perror("main(): socket()");
    exit(EXIT_FAILURE);
  }

  struct sockaddr_un sockaddr_forwarding;
  sockaddr_forwarding.sun_family = AF_UNIX;
  strncpy(sockaddr_forwarding.sun_path, forwarding_socketpath, sizeof(sockaddr_forwarding.sun_path)-1);

  if(bind(sd, (struct sockaddr *)&sockaddr_forwarding, sizeof(sockaddr_forwarding)) == -1){
    perror("main(): bind()");
    exit(EXIT_FAILURE);
  }

  if(listen(sd, 1) == -1){
    perror("main(): listen()");
    exit(EXIT_FAILURE);
  }

  ep_ctrl.accept_forwarding_fd = sd;

  struct epoll_event ev_f = {0};
  ev_f.events = EPOLLIN;
  ev_f.data.fd = sd;
  if (epoll_ctl(ep_ctrl.epoll_fd, EPOLL_CTL_ADD, ep_ctrl.accept_forwarding_fd, &ev_f) == -1) {
    perror("epoll_ctl: listen_sock");
    close(sd);
    exit(EXIT_FAILURE);
  }

  time_t time_since_last_updatemsg = time(NULL);
  while(1){
    int nr_fd_ready = epoll_wait(ep_ctrl.epoll_fd, ep_ctrl.events, MAX_EVENTS, 10000);
    if(nr_fd_ready == -1){
      perror("main(): epoll_wait()");
      close(sd);
      exit(EXIT_FAILURE);
    } else if(nr_fd_ready == 0){
      if(send_routing_info(&ep_ctrl) == -1){
        return EXIT_FAILURE;
      }
      time_since_last_updatemsg = time(NULL);
      check_timeout_neighbors(&ep_ctrl);
      continue;
    }
    int n;
    for (n = 0; n<nr_fd_ready; n++){
      int rv = epoll_event(&ep_ctrl, n);
      if(rv == -1){
        return EXIT_FAILURE;
      }
    }
    check_timeout_neighbors(&ep_ctrl);
    if(start_routing_process == 1){
      if(send_routing_info(&ep_ctrl) == -1){
        return EXIT_FAILURE;
      }
      start_routing_process = 0;
    }
    if((time(NULL)-time_since_last_updatemsg) > 10){
      if(send_routing_info(&ep_ctrl) == -1){
        return EXIT_FAILURE;
      }
      time_since_last_updatemsg = time(NULL);
    }
  }

  return EXIT_SUCCESS;
}
