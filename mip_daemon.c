#define _GNU_SOURCE
#include <sys/socket.h>
#include <sys/un.h>
#include <stdlib.h>
#include <sys/types.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/epoll.h>
#include <arpa/inet.h>
#include <net/if.h>
#include <net/ethernet.h>
#include <linux/if_packet.h>
#include <ifaddrs.h>
#include <sys/ioctl.h>
#include <inttypes.h>
#include "mip_header.h"

#define ETH_P_MIP 0x88B5
#define MIPARP_TABLE_SIZE 4
/* Masks used for bit operations */
#define DEST_MIP_MASK 534773760
#define SOURCE_MIP_MASK 2088960
#define TTL_MASK 15
#define PENDING_MSG_QUEUE_SIZE 50



/* an array of the mip addresses on this daemon */
int mipadr_on_this_daemon[MAX_INTERFACES] = {0};
/*
  the mip_arp_table which is used for storing info of the interfaces
  and are used for finding the right interface and socket when sending message_struct
*/
struct mip_arp_info mip_arp_table[MIPARP_TABLE_SIZE];
/* array which stores the raw socket epoll events */
struct epoll_event raw_events[MAX_EVENTS];

/*
  debugmode off = 0
  debugmode on = 1
*/
int debugmode = 0;

/*
   The mipaddress to the next host a msg is going to be sent to.
   If next_mip == -1, then mip_daemon have to ask routing_server
   which is the next hop.
*/
int next_mip = -1;

/*
  pending_msg_bool = 0 = no pending messages
  pending_msg_bool = 1 = there is a pending message in the pending_msg_from_client struct
*/
struct pending_msg_from_client_queue_struct pending_msg_from_client_queue[PENDING_MSG_QUEUE_SIZE];

/* 0 == not connetected, 1 == connected */
int client_or_server_connected = 0;

int send_msg_with_miparp(int mip_addr, char* buf, struct epoll_control *ep_ctrl, int tra_bits, int source_mip, size_t length);
int send_broadcast_msg(int mip_addr);

/*
  This function prints a debug message sent in by the parameter if the program
  is running in debug mode.
  The global variable "debugmode" is used here.

  Parameters:
    msg: The debug message
*/
void debug_print(char* msg){
  if(debugmode == 1){
    fprintf(stdout, "<<< DEBUG PRINT >>>  %s\n", msg);
  }
}

/*
  This function prints a debug message sent in by the parameters if the program
  is running in debug mode.
  The global variable "debugmode" is used here.

  Parameters:
    msg: The debug message
    v: an int value
*/
void debug_print_with_variable(char* msg, int v){
  if(debugmode == 1){
    fprintf(stdout, "<<< DEBUG PRINT >>>  %s %i\n", msg, v);
  }
}

/*
  This function prints a debug message of the destination mac-address if the program
  is running in debug mode.
  The global variable "debugmode" is used here.

  Parameters:
    frame: The struct which contains the mac address
*/
void debug_destmacaddr_print(struct ethernet_frame* frame){
  if(debugmode == 1){
    printf("<<< DEBUG PRINT >>>  Dest MAC: %02x:%02x:%02x:%02x:%02x:%02x\n",
    frame->dest_macaddr[0] & 0xff, frame->dest_macaddr[1] & 0xff, frame->dest_macaddr[2] & 0xff,
    frame->dest_macaddr[3] & 0xff, frame->dest_macaddr[4] & 0xff, frame->dest_macaddr[5] & 0xff);
  }
}

/*
  This function prints a debug message of the source mac-address if the program
  is running in debug mode.
  The global variable "debugmode" is used here.

  Parameters:
    frame: The struct which contains the mac address
*/
void debug_sourcemacaddr_print(struct ethernet_frame* frame){
  if(debugmode == 1){
    printf("<<< DEBUG PRINT >>>  Source MAC: %02x:%02x:%02x:%02x:%02x:%02x\n",
    frame->source_macaddr[0] & 0xff, frame->source_macaddr[1] & 0xff, frame->source_macaddr[2] & 0xff,
    frame->source_macaddr[3] & 0xff, frame->source_macaddr[4] & 0xff, frame->source_macaddr[5] & 0xff);
  }
}

/*
  This function prints a debug message of the source mac-address in the mip_arp_table
  if the program is running in debug mode.
  The global variable "debugmode" and "mip_arp_table" is used here.

  Parameters:
    i: The index to the table for getting the right struct
*/
void debug_sourcemacaddr_print_global(int i){
  if(debugmode == 1){
    fprintf(stdout, "<<< DEBUG PRINT >>>  Source MAC: %02x:%02x:%02x:%02x:%02x:%02x\n",
    mip_arp_table[i].source_macaddr[0] & 0xff, mip_arp_table[i].source_macaddr[1] & 0xff, mip_arp_table[i].source_macaddr[2] & 0xff,
    mip_arp_table[i].source_macaddr[3] & 0xff, mip_arp_table[i].source_macaddr[4] & 0xff, mip_arp_table[i].source_macaddr[5] & 0xff);
  }
}

/*
  This function prints a debug message of the destination mac-address in the mip_arp_table
  if the program is running in debug mode.
  The global variable "debugmode" and "mip_arp_table" is used here.

  Parameters:
    i: The index to the table for getting the right struct
*/
void debug_destmacaddr_print_global(int i){
  if(debugmode == 1){
    fprintf(stdout, "<<< DEBUG PRINT >>>  Dest MAC: %02x:%02x:%02x:%02x:%02x:%02x\n",
    mip_arp_table[i].dest_macaddr[0] & 0xff, mip_arp_table[i].dest_macaddr[1] & 0xff, mip_arp_table[i].dest_macaddr[2] & 0xff,
    mip_arp_table[i].dest_macaddr[3] & 0xff, mip_arp_table[i].dest_macaddr[4] & 0xff, mip_arp_table[i].dest_macaddr[5] & 0xff);
  }
}


/*
  This function prints the current state of the miparp-table, and it's used for debug.
  The global variable "mip_arp_table" is used here.
*/
void print_miparp_table(){
  printf("***************************************\n");
  int i;
  for(i = 0; i<MIPARP_TABLE_SIZE; i++){
    printf("<<< DEBUG PRINT >>>  Index: %i\n", i);
    printf("<<< DEBUG PRINT >>>  source_macaddr: ");
    int k;
    for (k = 0; k < 5; k++) {
      fprintf(stdout, "%x:", mip_arp_table[i].source_macaddr[k]);
    }
    fprintf(stdout, "%x\n", mip_arp_table[i].source_macaddr[5]);

    printf("<<< DEBUG PRINT >>>  dest_macaddr: ");
    for (k = 0; k < 5; k++) {
      fprintf(stdout, "%x:", mip_arp_table[i].dest_macaddr[k]);
    }
    fprintf(stdout, "%x\n", mip_arp_table[i].dest_macaddr[5]);

    printf("<<< DEBUG PRINT >>>  socket_fd: %d\n<<< DEBUG PRINT >>>  source_mip: %d\n<<< DEBUG PRINT >>>  dest_mip: %d\n", mip_arp_table[i].socket_fd, mip_arp_table[i].source_mip, mip_arp_table[i].dest_mip);
    printf("______________________________________\n");
  }
  printf("***************************************\n\n");

}


/*
  This function updates the miparp-table if the destination mip-address is equal
  to the source mip address on the socket it came on.
  The global variable "mip_arp_table" is used here.

  Parameters:
    frame: a struct of the frame that was received on the socket
    sfd: socket filedescriptor of the socket the framed was received on

  Return values:
    1: if the miparp-table has been updated
    0: if the destination mip address doesn't belong to the interface it received on
*/
int update_miparp(struct ethernet_frame* frame, int sfd){
  struct network_datagram* datagram = (struct network_datagram*)frame->msg;
  uint32_t network_header = datagram->network_header;
  uint32_t mip_dest_32 = (network_header & DEST_MIP_MASK)>>21;
  unsigned char mip_dest = (unsigned char) mip_dest_32;
  uint32_t mip_source_32 = (network_header & SOURCE_MIP_MASK)>>13;
  unsigned char mip_source = (unsigned char) mip_source_32;

  int i;
  for(i = 0; i<MIPARP_TABLE_SIZE; i++){
    if((mip_arp_table[i].socket_fd == sfd) && (mip_arp_table[i].source_mip == mip_dest)){
      mip_arp_table[i].dest_mip = mip_source;
      memcpy(mip_arp_table[i].dest_macaddr, frame->source_macaddr, 6);

      if(debugmode == 1){
       debug_print("MIPARP TABLE AFTER UPDATE");
       print_miparp_table();
      }

      return 1;
    }
  }
  return 0;
}

/*
  This function creates and sends a miparp response.
  The global variable "mip_arp_table" is used here.

  Parameters:
    sfd: socket file descriptor the message will be sent on.

  Return values:
    1: on success
    -1: on error
*/
int send_miparp_response(int sfd){
  int i;
  for(i = 0; i<MIPARP_TABLE_SIZE; i++){
    if(mip_arp_table[i].socket_fd == sfd){
      struct ethernet_frame new_frame;
      memcpy(new_frame.dest_macaddr, mip_arp_table[i].dest_macaddr, 6);
      memcpy(new_frame.source_macaddr, mip_arp_table[i].source_macaddr, 6);
      new_frame.protocol = htons(ETH_P_MIP);
      uint32_t network_header = (15 | (mip_arp_table[i].source_mip<<13));
      network_header = (network_header | (mip_arp_table[i].dest_mip<<21));
      memcpy(new_frame.msg, &network_header, 4);

      debug_print("SENDING MIPARP RESPONSE:");
      debug_destmacaddr_print(&new_frame);
      debug_sourcemacaddr_print(&new_frame);
      debug_print_with_variable("Dest MIP address: ", mip_arp_table[i].dest_mip);
      debug_print_with_variable("Source MIP address: ", mip_arp_table[i].source_mip);

      int rv = send(sfd, &new_frame, sizeof(struct ethernet_frame)+4, 0);
      //printf("Her har sendt: %i\n", rv);
      if(rv == -1){
        perror("main(): epoll_event(): check_eth_frame(): send_miparp_response()");
        return -1;
      }
      return 1;
    }
  }
  return -1;
}

/*
  This function checks if the frame was supposed to reach this daemon, and if it was
  then it updates the miparp and sends pending messages or miparp-response, dependig
  on the incoming frame.
  The global variable "mip_arp_table" is used here.

  Parameters:
    frame: a struct of the incoming frame
    sfd: socket file descriptor to the socket the frames came on
    ep_ctrl: a pointer to the epoll_control struct that contains info of the epoll

  Return values:
    4: if the this host dosen't have the dest mip address, and the message have to be forwarded
    3: if it was a message to the routing server: TRA-bits = 010
    2: if the message was meant for this daemon, and it has to be forwarded to
      either a local client or server
    1: if it was a broadcast or a miparp-response, and the miparp-table is updated
    0: if TTL has reached 0
    -1: on errors

*/
int check_eth_frame(struct ethernet_frame* frame, int sfd, struct epoll_control *ep_ctrl){
  struct network_datagram* datagram = (struct network_datagram*)frame->msg;
  uint32_t network_header = datagram->network_header;

  if((network_header & TTL_MASK) == 0){
    return 0;
  }

  if((network_header >> 29) == 2){
    struct iovec iov[1];

    iov[0].iov_base = datagram;
    iov[0].iov_len = sizeof(struct network_datagram)+(sizeof(struct dest_outgoing_cost)*MAX_TABLE_SIZE);

    struct msghdr message = {0};
    message.msg_iov = iov;
    message.msg_iovlen = 1;

    if(sendmsg(ep_ctrl->routing_fd, &message, 0) == -1){
      perror("main(): epoll_event(): check_eth_frame(): sendmsg()");
      free(datagram);
      return -1;
    }
    return 3;
  }

  int i, j;
  for(i = 0; i<6; i++){
    if(frame->dest_macaddr[i] != 255){
      for(j = 0; j<MIPARP_TABLE_SIZE; j++){

        if(memcmp(frame->dest_macaddr, mip_arp_table[j].source_macaddr, 6) == 0){

          debug_print("Received message from raw socket:");
          debug_destmacaddr_print(frame);
          debug_sourcemacaddr_print(frame);
          debug_print_with_variable("Dest MIP address: ", (network_header & DEST_MIP_MASK)>>21);
          debug_print_with_variable("Source MIP address: ", (network_header & SOURCE_MIP_MASK)>>13);
          debug_print("Current status of MIPARP table");
          if(debugmode == 1) print_miparp_table();


          if((network_header>>29) == 0){
            debug_print("The message was a MIPARP response, current status of MIPARP table after update:");
            if(update_miparp(frame, sfd) == -1){
              return -1;
            }

            int i;
            for(i = 0; i<PENDING_MSG_QUEUE_SIZE; i++){
              if(pending_msg_from_client_queue[i].pending_msg_bool == 1){
                debug_print("There was a pending message, and it will be sent now:");
                //printf("pending_msg_from_client->msg[50]: %c\n", pending_msg_from_client->msg[50]);
                if(pending_msg_from_client_queue[i].pending_msg_from_client->routing == 1){
                  send_msg_with_miparp(pending_msg_from_client_queue[i].pending_msg_from_client->mip_addr, pending_msg_from_client_queue[i].pending_msg_from_client->msg, ep_ctrl, 2, pending_msg_from_client_queue[i].pending_msg_from_client->source_mip, pending_msg_from_client_queue[i].pending_msg_from_client->length);
                } else{
                  send_msg_with_miparp(pending_msg_from_client_queue[i].pending_msg_from_client->mip_addr, pending_msg_from_client_queue[i].pending_msg_from_client->msg, ep_ctrl, 4, pending_msg_from_client_queue[i].pending_msg_from_client->source_mip, pending_msg_from_client_queue[i].pending_msg_from_client->length);
                }
                pending_msg_from_client_queue[i].pending_msg_bool = 0;
                free(pending_msg_from_client_queue[i].pending_msg_from_client);
              }
            }

            return 1;
          }

          //a check if this host has the destination mip address
          int k;
          for(k = 0; k<MAX_INTERFACES; k++){
            if((unsigned int)mipadr_on_this_daemon[k] == ((network_header & DEST_MIP_MASK)>>21)){
              return 2;
            }
          }



          return 4;
        }
      }
      return -1;
    }
  }

  debug_print("Received a broadcast message from raw socket:");
  debug_destmacaddr_print(frame);
  debug_sourcemacaddr_print(frame);
  debug_print_with_variable("Dest MIP address: ", (network_header & DEST_MIP_MASK)>>21);
  debug_print_with_variable("Source MIP address: ", (network_header & SOURCE_MIP_MASK)>>13);
  debug_print("Current status of MIPARP table before update");
  if(debugmode == 1) print_miparp_table();


  int ret = update_miparp(frame, sfd);
  if(ret == -1){
    return -1;
  } else if(ret == 0){

    return 0;
  } else{
    if(send_miparp_response(sfd) == -1){
      return -1;
    }
    return 1;
  }


  return 1;
}

/*
  This function adds an unix socket to the epoll.

  Parameters:
    ep_ctrl: a pointer to a epoll_control struct
    fd: the socket filedescriptor to the socket which is going to be added

  Return values:
    0: on success
    -1: on error
*/
int epoll_add(struct epoll_control *ep_ctrl, int fd){
  struct epoll_event ev = {0};
  //ev.events = EPOLLIN | EPOLLET;
  ev.events = EPOLLIN;
  ev.data.fd = fd;
  if (epoll_ctl(ep_ctrl->epoll_fd, EPOLL_CTL_ADD, fd, &ev) == -1) {
    perror("epoll_add(): epoll_ctl()");
    return -1;
  }
  return 0;

}

/*
  This function sends a message to the client/server which is connected via an unix socket.

  Parameters:
    sd: socket filedescriptor to the socket it's going to be sent on
    frame: the frame that was sent via a rawsocket

  Return values:
    1: if neither a client or server is connected
    0: on success
    -1: on errors
*/
int send_msg_unixsd(int sd, struct ethernet_frame * frame){
  if(client_or_server_connected == 0){
    fprintf(stderr, "Neither a client or server is connected to this daemon\n");
    return 1;
  }
  char buffer[1496] = {0};
  struct network_datagram* datagram = (struct network_datagram*)frame->msg;
  uint32_t network_header = datagram->network_header;
  int payload_length_mask = 8176;
  int length = (network_header & payload_length_mask) >> 4;
  memcpy(&buffer[0], datagram->msg, sizeof(char) * (length*4));

  int mip_addr = (network_header & SOURCE_MIP_MASK)>>13;

  struct iovec iov[3];
  iov[0].iov_base = buffer;
  iov[0].iov_len = sizeof(buffer);

  iov[1].iov_base = &mip_addr;
  iov[1].iov_len = sizeof(mip_addr);

  iov[2].iov_base = &length;
  iov[2].iov_len = sizeof(length);

  struct msghdr message = {0};
  message.msg_iov = iov;
  message.msg_iovlen = 3;

  if(debugmode == 1){
    fprintf(stdout, "<<< DEBUG PRINT >>>  MESSAGE TO UNIX SOCKET:\n<<< DEBUG PRINT >>>  Mip_addr: %d\n<<< DEBUG PRINT >>>  Msg: %s\n\n", mip_addr, &buffer[4]);
  }
  if(sendmsg(sd, &message, 0) == -1){
    perror("main(): epoll_event(): send_msg_unixsd()");
    return -1;
  }

  return 0;
}

/*
  This function creates a frame with the network header and message, plus
  the ethernet header.
  The global variable "mip_arp_table" is used here.

  Parameters:
    message: the message that's going to be sent
    index_miparp_table: the index to the miparp-table which contains all the
      info of the interface that is necessary
    tra_bits: Transport (bit combination 100), Routing (bit combination 010) or ARP broadcast message (bit combination 001)
    dest_mip: the destination mip address for the message

  Return values:
    A pointer to the ethernet_frame struct on success.
    NULL on error.
*/
struct ethernet_frame* create_frame(char* message, int index_miparp_table, int tra_bits, int dest_mip, int source_mip, size_t length_msg){
  uint32_t network_header = 0;
  size_t length = 0;
  if(length_msg%4 == 0){
    length = (length_msg)/4;
  } else{
    length = (length_msg + (4 - (length_msg%4)))/4;
  }
  if(tra_bits == 4){
    network_header = (15 | (length<<4) | (source_mip << 13) | (dest_mip << 21) | 4<< 29);
  } else if(tra_bits == 2){
    network_header = (15 | (length<<4) | (mip_arp_table[index_miparp_table].source_mip << 13) | (mip_arp_table[index_miparp_table].dest_mip << 21) | 2<< 29);
  }

  struct network_datagram* datagram = malloc(sizeof(struct network_datagram)+length_msg);
  if(datagram == NULL){
    return NULL;
  }
  datagram->network_header = network_header;

  memcpy(datagram->msg, message, length_msg);
  struct ethernet_frame* frame = malloc(sizeof(struct network_datagram)+length_msg+sizeof(struct ethernet_frame));
  if(frame == NULL){
    return NULL;
  }
  memcpy(frame->msg, datagram, length_msg+4);
  frame->protocol = htons(ETH_P_MIP);
  memcpy(frame->source_macaddr, mip_arp_table[index_miparp_table].source_macaddr, 6);
  memcpy(frame->dest_macaddr, mip_arp_table[index_miparp_table].dest_macaddr, 6);
  free(datagram);
  return frame;
}

/*
  This function asks the routing server which mip-address is the next hop.

  Parameters:
    dest_mip: the destination mip address for the message
    ep_ctrl: a pointer to the epoll_control struct that contains info of the epoll

  Return values:
    On success 0, on errors -1.
*/
int get_next_mip_hop(int dest_mip, struct epoll_control* ep_ctrl){

  int mip_addr = dest_mip;

  struct iovec iov[1];

  iov[0].iov_base = &mip_addr;
  iov[0].iov_len = sizeof(mip_addr);

  struct msghdr message = {0};
  message.msg_iov = iov;
  message.msg_iovlen = 1;

  if(sendmsg(ep_ctrl->forwarding_fd, &message, 0) == -1){
    perror("main(): epoll_event(): send_msg_with_miparp(): get_next_mip_hop(): sendmsg()");
    return -1;
  }

  return 0;
}

/*
  This function looks up in the miparp-table and sends an ethernet frame with
  the message from client/server if the table has the destination mipaddress.
  The global variable "mip_arp_table" is used here.

  Parameters:
    mip_addr: the destination mip-address
    message: the message from client/server
    ep_ctrl: a pointer to the epoll_control struct that contains info of the epoll
    tra_bits: Transport (bit combination 100), Routing (bit combination 010) or ARP broadcast message (bit combination 001)

  Return values:
    0: on success
    -1: on errors
    -2: the message could not be sent, and a broadcast is necessary
    -3: have asked routing server for the next hop, and the message have to be stored

*/
int send_msg_with_miparp(int mip_addr, char* message, struct epoll_control *ep_ctrl, int tra_bits, int source_mip, size_t length){
  if(next_mip == -1){
    int next_mip_addr = get_next_mip_hop(mip_addr, ep_ctrl);
    if(next_mip_addr == -1){
      return -1;
    }
    return -3;
  }
  int i;
  for(i = 0; i<MIPARP_TABLE_SIZE; i++){
    if(mip_arp_table[i].dest_mip == next_mip){
      struct ethernet_frame* frame = create_frame(message, i, tra_bits, mip_addr, source_mip, length);
      if(frame == NULL){
        return -1;
      }

      debug_print("Sending message via raw socket by using the MIPARP-table:");
      debug_sourcemacaddr_print_global(i);
      debug_destmacaddr_print_global(i);
      debug_print_with_variable("Source MIP-address: ", mip_arp_table[i].source_mip);
      debug_print_with_variable("Dest MIP-address: ", mip_arp_table[i].dest_mip);
      debug_print("Current status on MIPARP-table:");
      if(debugmode == 1) print_miparp_table();
      int rv = send(mip_arp_table[i].socket_fd, frame, sizeof(struct network_datagram)+length+sizeof(struct ethernet_frame), 0);
      if(rv == -1){
        perror("main(): epoll_event(): send_msg_with_miparp(): send()");
        free(frame);
        return -1;
      }
      next_mip = -1;

      free(frame);
      return 0;
    }
  }
  if(send_broadcast_msg(next_mip) == -1){
    return -1;
  }
  return -2;
}


/*
  This function sends out a broadcast message on all the raw sockets.
  The global variable "mip_arp_table" is used here.

  Parameters:
  mip_addr: the destination mip address

  Return values:
  0: on success
  -1: on errors
*/
int send_broadcast_msg(int mip_addr){
  int i;
  for(i = 0; i<MIPARP_TABLE_SIZE; i++){
    if(mip_arp_table[i].socket_fd != 0){
      struct network_datagram* datagram = malloc(sizeof(struct network_datagram));
      if(datagram == NULL){
        return -1;
      }

      uint32_t network_header = 0;
      network_header = (15 | (mip_arp_table[i].source_mip<<13) | (mip_addr<<21) | (1<<29));
      datagram->network_header = network_header;
      struct ethernet_frame* frame = malloc(sizeof(struct ethernet_frame) + sizeof(struct network_datagram));
      if(frame == NULL){
        free(datagram);
        return -1;
      }

      memcpy(frame->msg, datagram, 4);
      frame->protocol = htons(ETH_P_MIP);
      memcpy(frame->source_macaddr, mip_arp_table[i].source_macaddr, 6);
      memcpy(frame->dest_macaddr, "\xff\xff\xff\xff\xff\xff", 6);

      debug_print("Sending broadcast message:\n");
      debug_sourcemacaddr_print_global(i);
      debug_destmacaddr_print(frame);
      debug_print_with_variable("Source MIP-address: ", mip_arp_table[i].source_mip);
      debug_print_with_variable("Dest MIP-address: ", mip_addr);


      size_t ret = send(mip_arp_table[i].socket_fd, frame, sizeof(struct ethernet_frame) + sizeof(struct network_datagram), 0);

      if(ret == (size_t)-1){
        perror("main(): epoll_event(): send_broadcast_msg()");
        free(datagram);
        free(frame);
        return -1;
      }

      free(datagram);
      free(frame);
    }
  }

  debug_print("Status of MIP-ARP table after sending broadcast messages:\n");
  if(debugmode == 1){
    print_miparp_table();
  }
  return 0;
}


/*
  This function receives datagrams from routing server, and sends it as ethernet frames
  to the requested mip adresses. It's used for communication between the router servers.

  Parameters:
    ep_ctrl: a pointer to a epoll_control struct which contains info of the epoll

  Return values:
    It returns 0 on success, and -1 on errors.
*/
int recv_and_send_routing_info(struct epoll_control *ep_ctrl){
  struct network_datagram* datagram = malloc(sizeof(struct network_datagram)+(sizeof(struct dest_outgoing_cost)*MAX_TABLE_SIZE));
  if(datagram == NULL){
    return -1;
  }

  struct iovec iov[2];

  iov[0].iov_base = datagram;
  iov[0].iov_len = sizeof(struct network_datagram)+(sizeof(struct dest_outgoing_cost)*MAX_TABLE_SIZE);

  int32_t mipaddr_msg_is_going_to_be_sent_from = 0;
  iov[1].iov_base = &mipaddr_msg_is_going_to_be_sent_from;
  iov[1].iov_len = 4;

  struct msghdr message = {0};
  message.msg_iov = iov;
  message.msg_iovlen = 2;

  ssize_t rv = recvmsg(ep_ctrl->routing_fd, &message, 0);
  if(rv == -1){
    perror("main(): send_routing_info(): sendmsg()");
    free(datagram);
    return -1;
  } else if(rv == 0){
    fprintf(stderr, "Routing socket has performed an orderly shutdown\n");
    return 1;
  }

  struct ethernet_frame* frame = malloc(sizeof(struct ethernet_frame)+ sizeof(struct network_datagram)+(sizeof(struct dest_outgoing_cost)*MAX_TABLE_SIZE));
  memset(frame, 0, sizeof(struct ethernet_frame)+ sizeof(struct network_datagram)+(sizeof(struct dest_outgoing_cost)*MAX_TABLE_SIZE));
  memcpy(frame->msg, datagram, sizeof(struct network_datagram)+(sizeof(struct dest_outgoing_cost)*MAX_TABLE_SIZE));
  int i;
  for(i = 0; i<MIPARP_TABLE_SIZE; i++){
    if(mip_arp_table[i].source_mip == mipaddr_msg_is_going_to_be_sent_from){
      int rv = send(mip_arp_table[i].socket_fd, frame, sizeof(struct ethernet_frame)+ sizeof(struct network_datagram)+(sizeof(struct dest_outgoing_cost)*MAX_TABLE_SIZE), 0);
      if(rv == -1){
        perror("main(): epoll_event(): recv_and_send_routing_info(): send()");
        free(datagram);
        free(frame);
        return -1;
      }
      free(datagram);
      free(frame);
      return 0;
    }
  }


  free(datagram);
  free(frame);

  return 0;
}


/*
  This function is called when a filedescriptor is ready for I/O.
  It distinguish between raw sockets and unix sockets, and reads and writes
  from and to them. This function has the responsibility for checking what caused
  the requested I/O, and then forward messages if that is necessary.
  This function uses the pending_msg_from_client variable, if it is necessary to
  store a message until a miparp response arrives. It also changes client_or_server_connected
  when it's a connection or disconnetion.

  Parameters:
    ep_ctrl: a pointer to a epoll_control struct which contains info of the epoll
    n: the index in the events array in the ep_ctrl, for getting the right epoll_event struct

  Return values:
    -2: if the connected unix socket has disconnected
    -1: on errors
    0: on success and where the requested I/O was from the unix socket
    1: on success and where the requested I/O was from a raw socket
    2: where sockets has been disconnected

*/
int epoll_event(struct epoll_control *ep_ctrl, int n){
  //ETHERNET
  int i;
  for(i = 0; i < MAX_INTERFACES; i++){
    if(ep_ctrl->events[n].data.fd == ep_ctrl->accept_rawsockets[i]){
      //printf("EN ACCEPT RAWSOCKETS\n");
      char buf[sizeof(struct ethernet_frame) + sizeof(struct network_datagram) + 1496] = {0};
      ssize_t ret;
      ret = recv(ep_ctrl->events[n].data.fd, &buf, sizeof(buf), 0);
      if(ret == -1){
        perror("main(): epoll_event(): recv():");
        return -1;
      }
      struct ethernet_frame* frame = (struct ethernet_frame *) buf;

      ret = check_eth_frame(frame, ep_ctrl->events[n].data.fd, ep_ctrl);
      if(ret == 2){
        send_msg_unixsd(ep_ctrl->unix_sfd, frame);
        return 1;
      } else if(ret == 3 || ret == 1 || ret == 0){
        return 1;
      } else if(ret == 4){
        struct network_datagram* datagram_temp = (struct network_datagram*)frame->msg;
        int payload_length_mask = 8176;
        int length = ((datagram_temp->network_header & payload_length_mask) >> 4)*4;
        struct network_datagram* datagram = malloc(sizeof(struct network_datagram) + length);
        memset(datagram, 0, sizeof(struct network_datagram)+length);
        memcpy(datagram, frame->msg, sizeof(struct network_datagram)+length);
        uint32_t new_network_header = datagram->network_header - 1;
        datagram->network_header = new_network_header;
        int rv = send_msg_with_miparp((datagram->network_header & DEST_MIP_MASK)>>21, datagram->msg, ep_ctrl, 4, (datagram->network_header & SOURCE_MIP_MASK)>>13, length); //length er vel riktig her??
        if(rv == -1){
          free(datagram);
          return -1;
        }else if(rv == -2 || rv == -3){
          int i;
          for(i = 0; i<PENDING_MSG_QUEUE_SIZE; i++){
            if(pending_msg_from_client_queue[i].pending_msg_bool == 0){
              pending_msg_from_client_queue[i].pending_msg_from_client = malloc(sizeof(struct pend_msg_from_client) + length + 1);
              memset(pending_msg_from_client_queue[i].pending_msg_from_client, 0, sizeof(struct pend_msg_from_client) + length + 1);
              memcpy(pending_msg_from_client_queue[i].pending_msg_from_client->msg, datagram->msg, length);
              pending_msg_from_client_queue[i].pending_msg_from_client->msg[length] = '\0';
              pending_msg_from_client_queue[i].pending_msg_from_client->length = length;
              pending_msg_from_client_queue[i].pending_msg_from_client->mip_addr = (datagram->network_header & DEST_MIP_MASK)>>21;
              pending_msg_from_client_queue[i].pending_msg_from_client->source_mip = (datagram->network_header & SOURCE_MIP_MASK)>>13;
              pending_msg_from_client_queue[i].pending_msg_from_client->routing = 0;
              pending_msg_from_client_queue[i].pending_msg_bool = 1;
              break;
            }
          }
        }
        free(datagram);
        return 1;
      }

      return -1;
    }
  }


  //LOCAL


  if(ep_ctrl->events[n].data.fd == ep_ctrl->client_accept_fd){
    //on client accept socket
    int connect_sd;
    struct sockaddr_un sockaddr = {0};
    sockaddr.sun_family = AF_UNIX;
    socklen_t addrlen = sizeof(sockaddr);
    connect_sd = accept(ep_ctrl->client_accept_fd, (struct sockaddr *)&sockaddr, &addrlen);
    if(connect_sd == -1){
      perror("epoll_event(): accept()");
      return -1;
    }
    ep_ctrl->unix_sfd = connect_sd;


    int rv = epoll_add(ep_ctrl, connect_sd);
    if(rv == -1){
      return -1;
    }
    client_or_server_connected = 1;
    debug_print("New unix socket connection\n\n");
  } else if(ep_ctrl->events[n].data.fd == ep_ctrl->routing_fd){
    //on routing socket
    int rv = recv_and_send_routing_info(ep_ctrl);
    if(rv == -1){
      return -1;
    } else if(rv == 1){
      if(epoll_ctl(ep_ctrl->epoll_fd, EPOLL_CTL_DEL, ep_ctrl->routing_fd, &ep_ctrl->events[n]) == -1){
        perror("main(): epoll_event(): epoll_ctl():");
        return -1;
      }
      return 2;
    }
    return 0;
  } else if(ep_ctrl->events[n].data.fd == ep_ctrl->forwarding_fd){
    //on forwarding socket
    struct iovec iov[1];

    iov[0].iov_base = &next_mip;
    iov[0].iov_len = sizeof(next_mip);

    struct msghdr message = {0};
    message.msg_iov = iov;
    message.msg_iovlen = 1;

    int rv = recvmsg(ep_ctrl->forwarding_fd, &message, 0);
    if(rv == -1){
      perror("main(): epoll_event(): send_msg_with_miparp(): get_next_mip_hop(): recvmsg()");
      return -1;
    } else if(rv == 0){
      printf("Forwarding socket has performed an orderly shutdown\n");
      if(epoll_ctl(ep_ctrl->epoll_fd, EPOLL_CTL_DEL, ep_ctrl->forwarding_fd, &ep_ctrl->events[n]) == -1){
        perror("main(): epoll_event(): epoll_ctl():");
        return -1;
      }
      return 2;
    }

    int temp_next_mip = next_mip;
    int i;
    for(i = 0; i<PENDING_MSG_QUEUE_SIZE; i++){
      if(pending_msg_from_client_queue[i].pending_msg_bool != 0){
        next_mip = temp_next_mip;
        rv = send_msg_with_miparp(pending_msg_from_client_queue[i].pending_msg_from_client->mip_addr, pending_msg_from_client_queue[i].pending_msg_from_client->msg, ep_ctrl, 4, pending_msg_from_client_queue[i].pending_msg_from_client->source_mip, pending_msg_from_client_queue[i].pending_msg_from_client->length);
      }
      if(rv == -1){
        return -1;
      } else if(rv == -2){
        return 0;
      }
      if(pending_msg_from_client_queue[i].pending_msg_bool != 0){
        pending_msg_from_client_queue[i].pending_msg_bool = 0;
        free(pending_msg_from_client_queue[i].pending_msg_from_client);
      }
    }
    return 0;
  } else{
    //on client/server socket
    char buf[1496];
    memset(buf, 0, sizeof(char)*1496);

    int mip_addr = 0;
    size_t length = 0;

    struct iovec iov[3];
    iov[0].iov_base = buf;
    iov[0].iov_len = sizeof(char)*1496;

    iov[1].iov_base = &mip_addr;
    iov[1].iov_len = sizeof(mip_addr);

    iov[2].iov_base = &length;
    iov[2].iov_len = sizeof(size_t);

    struct msghdr message = {0};
    message.msg_iov = iov;
    message.msg_iovlen = 3;

    ssize_t rv = recvmsg(ep_ctrl->events[n].data.fd, &message, 0);
    if (rv == 0) {
      if(epoll_ctl(ep_ctrl->epoll_fd, EPOLL_CTL_DEL, ep_ctrl->client_accept_fd, &ep_ctrl->events[n]) == -1){
        perror("main(): epoll_event(): epoll_ctl():");
        return -1;
      }
      client_or_server_connected = 0;
      debug_print("Unix socket connection closed");

      return -2;
    } else if (rv == -1) {
      perror("epoll_event: recv()");
      return -1;
    } else if(rv > 1508){
      fprintf(stderr, "The message is too large\n");
      return -1;
    }

    if(debugmode == 1){
      fprintf(stdout, "<<< DEBUG PRINT >>>  MESSAGE FROM UNIX SOCKET:\n<<< DEBUG PRINT >>>  Mip_addr: %d\n<<< DEBUG PRINT >>>  Msg: %s\n\n", mip_addr, &buf[4]);
    }

    rv = send_msg_with_miparp(mip_addr, buf, ep_ctrl, 4, mipadr_on_this_daemon[0], length);
    if(rv == -1){
      return -1;
    } else if(rv == -2 || rv == -3){
      int i;
      for(i = 0; i<PENDING_MSG_QUEUE_SIZE; i++){
        if(pending_msg_from_client_queue[i].pending_msg_bool == 0){
          pending_msg_from_client_queue[i].pending_msg_from_client = malloc(sizeof(struct pend_msg_from_client) + length + 1); //trenger +1??
          memset(pending_msg_from_client_queue[i].pending_msg_from_client, 0, sizeof(struct pend_msg_from_client) + length + 1);
          memcpy(pending_msg_from_client_queue[i].pending_msg_from_client->msg, buf, length);
          pending_msg_from_client_queue[i].pending_msg_from_client->msg[length] = '\0';
          pending_msg_from_client_queue[i].pending_msg_from_client->length = length;
          pending_msg_from_client_queue[i].pending_msg_from_client->mip_addr = mip_addr;
          pending_msg_from_client_queue[i].pending_msg_from_client->source_mip = mipadr_on_this_daemon[0];
          pending_msg_from_client_queue[i].pending_msg_from_client->routing = 0;
          pending_msg_from_client_queue[i].pending_msg_bool = 1;
          break;
        }
      }

    }

  }
  return 0;
}

/*
  This function sends the mipadr_on_this_daemon array to the routing server.

  Parameters:
    ep_ctrl: a pointer to a epoll_control struct which contains info of the epoll

  Return values:
    0 on success, -1 on errors.
*/
int send_mipadr_info_to_routing_server(struct epoll_control *ep_ctrl){
  struct iovec iov[2];
  int mipadr_info_msg = -1; //so routing server knows that this is a mipadr info msg
  iov[0].iov_base = &mipadr_info_msg;
  iov[0].iov_len = sizeof(mipadr_info_msg);

  iov[1].iov_base = mipadr_on_this_daemon;
  iov[1].iov_len = sizeof(mipadr_on_this_daemon);

  struct msghdr message;
  memset(&message, 0, sizeof(message));
  message.msg_iov = iov;
  message.msg_iovlen = 2;

  ssize_t rv = sendmsg(ep_ctrl->forwarding_fd, &message, 0);
  if(rv == -1){
    perror("main(): send_mipadr_info_to_routing_server(): sendmsg()");
    return -1;
  }
  return 0;
}

/*
  This function puts in mip addresses, typed in by the user, in the mipadr_on_this_daemon table
  which is a global variable.

  Parameters:
    argc: number of arguments when starting the program
    argv: a string array with the command line arguments
*/
void init_mipadr_on_this_daemon(int argc, char* argv[]){
  int num_mipaddr;
  int debug = strcmp(argv[1], "-d");
  if(debug == 0){
    num_mipaddr = argc - 5;
  } else{
    num_mipaddr = argc - 4;
  }
  int i;
  if(debug == 0){
    for(i = 0; i<num_mipaddr; i++){
      mipadr_on_this_daemon[i] = atoi(argv[5+i]);
    }
  } else{
    for(i = 0; i<num_mipaddr; i++){
      mipadr_on_this_daemon[i] = atoi(argv[4+i]);
    }
  }
}

/*
  This checks if the number of command line arguments are correct,
  and activate debugmode or print help message if that is necessary.

  Parameters:
    argc: number of arguments when starting the program
    argv: a string array with the command line arguments
*/
void check_input_parameters(int argc, char* argv[]){
  if(argc < 5){
    fprintf(stderr, "Usage: %s [-d] <transport daemon socketpath> <routing socketpath> <forwarding socketpath> [MIP adresses ...]\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  if(strcmp(argv[1], "-h") == 0){
    fprintf(stdout, "Usage: %s <transport daemon socketpath> <routing socketpath> <forwarding socketpath> [MIP adresses ...]\n", argv[0]);
    exit(EXIT_SUCCESS);
  } else if(strcmp(argv[1], "-d") == 0){
    fprintf(stdout, "Debug print is activated\n");
    debugmode = 1;
    if(argc < 6){
      fprintf(stderr, "MIP adresses is necessary to start the daemon\n");
      fprintf(stderr, "Usage: %s [-d] <transport daemon socketpath> <routing socketpath> <forwarding socketpath> [MIP adresses ...]\n", argv[0]);
      exit(EXIT_FAILURE);
    }
  } else{
    if(argc < 5){
      fprintf(stderr, "MIP adresses is necessary to start the daemon\n");
      fprintf(stderr, "Usage: %s [-d] <transport daemon socketpath> <routing socketpath> <forwarding socketpath> [MIP adresses ...]\n", argv[0]);
      exit(EXIT_FAILURE);
    }
  }
}


int main(int argc, char* argv[]){
  int sd = 0;
  struct epoll_control ep_ctrl = {0};
  check_input_parameters(argc, argv);
  memset(&mip_arp_table, 0, sizeof(mip_arp_table));

  init_mipadr_on_this_daemon(argc, argv);

  memset(pending_msg_from_client_queue, 0, sizeof(pending_msg_from_client_queue));

  char* client_socketpath = {0};
  char* routing_socketpath = {0};
  char* forwarding_socketpath = {0};

  if(debugmode == 1){
    client_socketpath = argv[2];
    routing_socketpath = argv[3];
    forwarding_socketpath = argv[4];
  } else{
    client_socketpath = argv[1];
    routing_socketpath = argv[2];
    forwarding_socketpath = argv[3];
  }

  sd = socket(AF_UNIX, SOCK_SEQPACKET, 0);
  if(sd == -1){
    perror("main(): socket()");
    exit(EXIT_FAILURE);
  }


  struct sockaddr_un sockaddr = {0};
  sockaddr.sun_family = AF_UNIX;
  strncpy(sockaddr.sun_path, client_socketpath, sizeof(sockaddr.sun_path)-1);

  if(bind(sd, (struct sockaddr *)&sockaddr, sizeof(sockaddr)) == -1){
    perror("main(): bind()");
    exit(EXIT_FAILURE);
  }

  if(listen(sd, 10) == -1){
    perror("main(): listen()");
    exit(EXIT_FAILURE);
  }

  ep_ctrl.client_accept_fd = sd;
  ep_ctrl.epoll_fd = epoll_create(99);

  if(ep_ctrl.epoll_fd == -1) {
    perror("main(): epoll_create()");
    close(sd);
    exit(EXIT_FAILURE);
  }

  struct epoll_event ev = {0};
  ev.events = EPOLLIN;
  ev.data.fd = sd;
  if (epoll_ctl(ep_ctrl.epoll_fd, EPOLL_CTL_ADD, ep_ctrl.client_accept_fd, &ev) == -1) {
    perror("epoll_ctl: listen_sock");
    close(sd);
    exit(EXIT_FAILURE);
  }


  int routing_sd = socket(AF_UNIX, SOCK_SEQPACKET, 0);
  if(routing_sd == -1){
    perror("main(): socket()");
    close(sd);
    exit(EXIT_FAILURE);
  }

  struct sockaddr_un sockaddr_routing = {0};
  sockaddr_routing.sun_family = AF_UNIX;
  strncpy(sockaddr_routing.sun_path, routing_socketpath, sizeof(sockaddr_routing.sun_path)-1);

  if(connect(routing_sd, (struct sockaddr*)&sockaddr_routing, sizeof(sockaddr_routing)) == -1){
    perror("main(): connect():");
    close(sd);
    close(routing_sd);
    exit(EXIT_FAILURE);
  }

  int forwarding_sd = socket(AF_UNIX, SOCK_SEQPACKET, 0);
  if(forwarding_sd == -1){
    perror("main(): socket()");
    close(sd);
    close(routing_sd);
    exit(EXIT_FAILURE);
  }

  struct sockaddr_un sockaddr_forwarding = {0};
  sockaddr_forwarding.sun_family = AF_UNIX;
  strncpy(sockaddr_forwarding.sun_path, forwarding_socketpath, sizeof(sockaddr_forwarding.sun_path)-1);

  if(connect(forwarding_sd, (struct sockaddr*)&sockaddr_forwarding, sizeof(sockaddr_forwarding)) == -1){
    perror("main(): connect():");
    close(sd);
    close(routing_sd);
    close(forwarding_sd);
    exit(EXIT_FAILURE);
  }

  ep_ctrl.routing_fd = routing_sd;
  ep_ctrl.forwarding_fd = forwarding_sd;

  struct epoll_event ev_r = {0};
  ev_r.events = EPOLLIN;
  ev_r.data.fd = routing_sd;
  if(epoll_ctl(ep_ctrl.epoll_fd, EPOLL_CTL_ADD, routing_sd, &ev_r) == -1){
    perror("main(): epoll_ctl()");
    return EXIT_FAILURE;
  }

  struct epoll_event ev_f = {0};
  ev_f.events = EPOLLIN;
  ev_f.data.fd = forwarding_sd;
  if(epoll_ctl(ep_ctrl.epoll_fd, EPOLL_CTL_ADD, forwarding_sd, &ev_f) == -1){
    perror("main(): epoll_ctl()");
    return EXIT_FAILURE;
  }


  char *filnavn = "socket.sock";
	unlink(filnavn);

  struct ifaddrs* ifaddr = {0};
  if (getifaddrs(&ifaddr) == -1) {
    perror("main(): getifaddrs()");
    close(sd);
    exit(EXIT_FAILURE);
  }
  struct ifaddrs *ifstart = ifaddr;
  int temp_ev_num = 0;
  while (ifaddr != NULL) {
    if(strcmp(ifaddr->ifa_name, "lo") != 0){
      struct sockaddr *addr = ifaddr->ifa_addr;
      if(addr->sa_family == AF_PACKET){
        raw_events[temp_ev_num].events = EPOLLIN;
        ep_ctrl.accept_rawsockets[temp_ev_num] = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
        raw_events[temp_ev_num].data.fd = ep_ctrl.accept_rawsockets[temp_ev_num];
        struct sockaddr_ll sockaddr_raw;
        memset(&sockaddr_raw, 0, sizeof(sockaddr_raw));
        sockaddr_raw.sll_family = AF_PACKET;
        sockaddr_raw.sll_ifindex = if_nametoindex(ifaddr->ifa_name);

        struct ifreq dev;
        strcpy(dev.ifr_name, ifaddr->ifa_name);
        if(ioctl(ep_ctrl.accept_rawsockets[temp_ev_num], SIOCGIFHWADDR, &dev) == -1 ) {
          perror("main(): ioctl()");
          exit(EXIT_FAILURE);
        }
        memcpy(mip_arp_table[temp_ev_num].source_macaddr, dev.ifr_hwaddr.sa_data, 6);

        mip_arp_table[temp_ev_num].socket_fd = ep_ctrl.accept_rawsockets[temp_ev_num];
        mip_arp_table[temp_ev_num].source_mip = mipadr_on_this_daemon[temp_ev_num];


        if (bind(ep_ctrl.accept_rawsockets[temp_ev_num], (struct sockaddr*)&sockaddr_raw, sizeof(sockaddr_raw)) == -1) {
          perror("main: bind()");
          exit(EXIT_FAILURE);
        }
        if (epoll_ctl(ep_ctrl.epoll_fd, EPOLL_CTL_ADD, ep_ctrl.accept_rawsockets[temp_ev_num], &raw_events[temp_ev_num]) == -1) {
          perror("epoll_ctl: listen_sock");
          close(sd);
          exit(EXIT_FAILURE);
        }
      }
      temp_ev_num++;
    }
		ifaddr = ifaddr->ifa_next;
	}
	freeifaddrs(ifstart);

  debug_print("MIP-ARP table after initialization");
  if(debugmode == 1){
    print_miparp_table();
  }


  if(send_mipadr_info_to_routing_server(&ep_ctrl) == -1){
    return EXIT_FAILURE;
  }

  while(1){
    int nr_fd_ready = epoll_wait(ep_ctrl.epoll_fd, ep_ctrl.events, MAX_EVENTS, -1);
    if(nr_fd_ready == -1){
      perror("main(): epoll_wait()");
      close(sd);
      exit(EXIT_FAILURE);
    }
    int n;
    for (n = 0; n<nr_fd_ready; n++){
      int rv = epoll_event(&ep_ctrl, n);

      if(rv == -2){
        if(listen(sd, 10) == -1){
          perror("main(): epoll_event(): listen()");
          exit(EXIT_FAILURE);
        }
        if (epoll_ctl(ep_ctrl.epoll_fd, EPOLL_CTL_ADD, ep_ctrl.client_accept_fd, &ev) == -1) {
          perror("epoll_ctl: listen_sock");
          close(sd);
          exit(EXIT_FAILURE);
        }

      } else if(rv == -1){
        return EXIT_SUCCESS;
      } else if(rv == 2){
        printf("No routing server is connected. Exits program\n");
        return EXIT_SUCCESS;
      }
    }

  }

  close(sd);

  return EXIT_SUCCESS;
}
