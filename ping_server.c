#include <sys/socket.h>
#include <sys/un.h>
#include <stdlib.h>
#include <sys/types.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

/*
  This checks if the number of command line arguments are correct,
  and print help message if that is necessary.

  Parameters:
    argc: number of arguments when starting the program
    argv: a string array with the command line arguments
*/
void check_input_parameters(int argc, char* argv[]){
  if(argc < 2){
    fprintf(stderr, "Usage: %s <socket application>\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  if(strcmp(argv[1], "-h") == 0){
    fprintf(stdout, "Usage: %s <socket application>\n", argv[0]);
    exit(EXIT_SUCCESS);
  } else{
    if(argc != 2){
      fprintf(stderr, "Usage: %s <socket application>\n", argv[0]);
      exit(EXIT_FAILURE);
    }
  }
}

int main(int argc, char* argv[]){
  check_input_parameters(argc, argv);
  char* socketpath = argv[1];
  int sd = socket(AF_UNIX, SOCK_SEQPACKET, 0);
  if(sd == -1){
    perror("main(): socket():");
    exit(EXIT_FAILURE);
  }

  struct sockaddr_un sockaddr;
  sockaddr.sun_family = AF_UNIX;
  strncpy(sockaddr.sun_path, socketpath, sizeof(sockaddr.sun_path)-1);

  if(connect(sd, (struct sockaddr*)&sockaddr, sizeof(sockaddr)) == -1){
    perror("main(): connect():");
    close(sd);
    exit(EXIT_FAILURE);
  }

  /*
    It receives the message and print it out, then it sends "Pong" back to the
    mip_daemon, and the mip_address is still the original mip address.
    After sending it goes back to the blocking recvmsg.
  */
  while(1){
    char buf[1496] = {0};
    int mip_address;

    struct iovec iov[2];
    iov[0].iov_base = buf;
    iov[0].iov_len = sizeof(buf);

    iov[1].iov_base = &mip_address;
    iov[1].iov_len = sizeof(mip_address);

    struct msghdr message;
    memset(&message, 0, sizeof(message));
    message.msg_iov = iov;
    message.msg_iovlen = 2;


    ssize_t rv = recvmsg(sd, &message, 0);
    if(rv == -1){
      perror("main(): recvmsg()");
      close(sd);
      exit(EXIT_FAILURE);
    } else if(rv == 0){
      fprintf(stderr, "The peer has performed an orderly shutdown\n");
      close(sd);
      exit(EXIT_FAILURE);
    }
    fprintf(stdout, "Message from client: %s\n", buf);

    memset(&buf[0], 0, sizeof(buf));
    strcpy(buf, "Pong");
    rv = sendmsg(sd, &message, 0);
    if(rv == -1){
      perror("main(): sendmsg()");
      close(sd);
      exit(EXIT_FAILURE);
    }
  }

  return EXIT_SUCCESS;
}
