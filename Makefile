CC=gcc
CFLAGS=-Wall -Wextra -std=gnu99 -g

all: mip_daemon routing_server transport_daemon file_transfer_server file_transfer_client


mip_daemon: mip_daemon.c
	$(CC) $(CFLAGS) $^ -o $@

routing_server: routing_server.c
	$(CC) $(CFLAGS) $^ -o $@

transport_daemon: transport_daemon.c
	$(CC) $(CFLAGS) $^ -o $@

file_transfer_server: file_transfer_server.c
	$(CC) $(CFLAGS) $^ -o $@

file_transfer_client: file_transfer_client.c
	$(CC) $(CFLAGS) $^ -o $@

clean:
		rm -f mip_daemon routing_server transport_daemon file_transfer_server file_transfer_client

clean_sockpath:
	rm -f A_rsp A_fsp A_tsp A_asp B_rsp B_fsp B_tsp B_asp
