#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <sys/un.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdint.h>
#include <sys/epoll.h>
#include "mip_header.h"


int main(int argc, char* argv[]){
  if(argc != 5){
    printf("usage: %s <socketpath> <filename> <destination MIP address> <port number>\n", argv[0]);
    return EXIT_SUCCESS;
  }

  char* socketpath = argv[1];
  int sd = socket(AF_UNIX, SOCK_SEQPACKET, 0);
  if(sd == -1){
    perror("main(): socket():");
    exit(EXIT_FAILURE);
  }

  struct sockaddr_un sockaddr;
  sockaddr.sun_family = AF_UNIX;
  strncpy(sockaddr.sun_path, socketpath, sizeof(sockaddr.sun_path)-1);

  if(connect(sd, (struct sockaddr*)&sockaddr, sizeof(sockaddr)) == -1){
    perror("main(): connect():");
    close(sd);
    exit(EXIT_FAILURE);
  }

  //sends info to the transport daemon of this app
  struct app_info* app_inf = malloc(sizeof(struct app_info));
  app_inf->port_number = atoi(argv[4]);
  app_inf->dest_mip = atoi(argv[3]);
  int rv = send(sd, app_inf, sizeof(struct app_info), 0);
  if(rv == -1){
    perror("main(): send()");
    return EXIT_FAILURE;
  }
  free(app_inf);

  //opens the file for reading
  FILE* file_pointer = fopen(argv[2], "r");
  if(file_pointer == NULL){
    perror("main(): fopen()");
    return EXIT_FAILURE;
  }

  /*
    The way I have read the file to a buffer is taken from this address:
    https://stackoverflow.com/questions/2029103/correct-way-to-read-a-text-file-into-a-buffer-in-c
  */
  char file_buffer[65535] = {0};
  size_t file_length = fread(file_buffer, sizeof(char), 65535, file_pointer);
  if(ferror(file_pointer) != 0){
    fprintf(stderr, "Error while reading file");
    return EXIT_FAILURE;
  }

  fclose(file_pointer);

  //sends the file and filesize as a "struct file_struct"
  struct file_struct* file_struct = malloc(sizeof(struct file_struct));
  memset(file_struct, 0, sizeof(struct file_struct));
  file_struct->file_size = file_length;
  memcpy(file_struct->file, file_buffer, file_length);

  rv = send(sd, file_struct, sizeof(struct file_struct), 0);
  if(rv == -1){
    perror("main(): send():");
    return EXIT_FAILURE;
  }


  free(file_struct);

  /* waits for a response from transport daemon whether it not could connect to
     server side, or if the file transfer was successfully */
  char message_from_transport_daemon[100] = {0};
  rv = recv(sd, message_from_transport_daemon, sizeof(message_from_transport_daemon), 0);
  if(rv == 0){
    printf("The transport daemon has performed an orderly shutdown\n");
    close(sd);
    return EXIT_SUCCESS;
  } else if(rv == -1){
    perror("main(): recv()");
    close(sd);
    return EXIT_FAILURE;
  }
  printf("%s\n", message_from_transport_daemon);
  close(sd);




}
