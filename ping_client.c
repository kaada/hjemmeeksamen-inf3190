#include <string.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <stdlib.h>
#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>
#include <time.h>

/*
  This checks if the number of command line arguments are correct,
  and print help message if that is necessary.

  Parameters:
    argc: number of arguments when starting the program
    argv: a string array with the command line arguments
*/
void check_input_parameters(int argc, char* argv[]){
  if(argc < 4){
    fprintf(stderr, "Usage: %s [-h] <destination host> <message> <socket application>\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  if(strcmp(argv[1], "-h") == 0){
    fprintf(stdout, "Usage: %s <destination host> <message> <socket application>\n", argv[0]); //ha med "-d" her?
    exit(EXIT_SUCCESS);
  } else{
    if(argc != 4){
      fprintf(stderr, "Usage: %s [-h] <destination host> <message> <socket application>\n", argv[0]);
      exit(EXIT_FAILURE);
    }
  }
}


int main(int argc, char* argv[]){
  check_input_parameters(argc, argv);
  char* socketpath = argv[3];
  char* message = argv[2];
  char buffer[1496] = {0};
  strncpy(buffer, message, strlen(message));

  int sd = socket(AF_UNIX, SOCK_SEQPACKET, 0);
  if(sd == -1){
    perror("main(): socket()");
    exit(EXIT_FAILURE);
  }

  struct sockaddr_un sockaddr;
  sockaddr.sun_family = AF_UNIX;
  strncpy(sockaddr.sun_path, socketpath, sizeof(sockaddr.sun_path)-1);


  if(connect(sd, (struct sockaddr*)&sockaddr, sizeof(sockaddr)) == -1){
    perror("main(): connect():");
    close(sd);
    exit(EXIT_FAILURE);
  }


  struct timeval timeout;
  timeout.tv_sec = 1;
  timeout.tv_usec = 0;

  //sets timeout on the socket
  if(setsockopt(sd, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(timeout)) == -1){
    perror("main(): setsockopt():");
    close(sd);
    return EXIT_FAILURE;
  }


  int mip_addr = atoi(argv[1]);
  struct iovec iov[2];
  iov[0].iov_base = buffer;
  iov[0].iov_len = sizeof(buffer);

  iov[1].iov_base = &mip_addr;
  iov[1].iov_len = sizeof(mip_addr);

  struct msghdr message_struct;
  memset(&message_struct, 0, sizeof(message_struct));
  message_struct.msg_iov = iov;
  message_struct.msg_iovlen = 2;

  //starts the clock, for the ping-value
  clock_t start = clock();
  ssize_t rv = sendmsg(sd, &message_struct, 0);
  if(rv == -1){
    perror("main(): sendmsg()");
    close(sd);
    exit(EXIT_FAILURE);
  }

  rv = recvmsg(sd, &message_struct, 0);
  clock_t end = clock();
  if(rv == -1){
    printf("Timeout\n");
    close(sd);
    exit(EXIT_FAILURE);
  } else if(rv == 0){
    fprintf(stderr, "The peer has performed an orderly shutdown\n");
    close(sd);
    exit(EXIT_FAILURE);
  }
  fprintf(stdout, "Message from server: %s\n", buffer);
  fprintf(stdout, "Time: %f\n", (double)(end-start));
  close(sd);
  return EXIT_SUCCESS;
}
