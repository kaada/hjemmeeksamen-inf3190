#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <sys/un.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/epoll.h>
#include "mip_header.h"

/*
  The functions "strreverse" and "itoa" is used to convert an integer to ascii,
  which is not supported in GNU99 which I use.
  I got it from this address: https://gist.github.com/mandrews/200846
*/
void strreverse(char* begin, char* end){
	char aux;
	while(end>begin)
		aux=*end, *end--=*begin, *begin++=aux;
}
void itoa(int value, char* str, int base) {
	static char num[] = "0123456789abcdefghijklmnopqrstuvwxyz";
	char* wstr=str;
	int sign;
	// Validate base
	if (base<2 || base>35){ *wstr='\0'; return; }
	// Take care of sign
	if ((sign=value) < 0) value = -value;
	// Conversion. Number is reversed.
	do *wstr++ = num[value%base]; while(value/=base);
	if(sign<0) *wstr++='-';
	*wstr='\0';
	// Reverse string
	strreverse(str,wstr-1);
}

/*
  This function opens a new file with writing permission, and the filename is
  a combination of the portnumber and how many files this port has written to earlier.

  Parameters:
    num_files_written_to: a counter of how many files this port has written to
    portnr: the port number the server listens to

  Return values:
    On errors it returns NULL, on success it returns a file pointer to the file.
*/
FILE* open_new_file(int num_files_written_to, char* portnr){
  char file_name[30] = {0};
  strcpy(file_name, "ReceivedFileOnPort");
  strcat(file_name, portnr);
  char* s = "Nr";
  strcat(file_name, s);
  char* num = malloc(4);
  itoa(num_files_written_to, num, 10);
  strcat(file_name, num);

  FILE* fp = fopen(file_name, "w");
  if(fp == NULL){
    perror("main(): open_new_file(): fopen()");
    free(num);
    return NULL;
  }
  free(num);
  return fp;
}

int main(int argc, char* argv[]){
  if(argc != 3){
    printf("usage: %s <socketpath> <portnr>\n", argv[0]);
    return EXIT_SUCCESS;
  }
  char* socketpath = argv[1];
  int sd = socket(AF_UNIX, SOCK_SEQPACKET, 0);
  if(sd == -1){
    perror("main(): socket():");
    exit(EXIT_FAILURE);
  }

  struct sockaddr_un sockaddr;
  sockaddr.sun_family = AF_UNIX;
  strncpy(sockaddr.sun_path, socketpath, sizeof(sockaddr.sun_path)-1);

  if(connect(sd, (struct sockaddr*)&sockaddr, sizeof(sockaddr)) == -1){
    perror("main(): connect():");
    close(sd);
    exit(EXIT_FAILURE);
  }

  //sends info to the transport daemon of this app
  struct app_info* app_inf = malloc(sizeof(struct app_info));
  app_inf->port_number = atoi(argv[2]);
  app_inf->dest_mip = -1;
  int rv = send(sd, app_inf, sizeof(struct app_info), 0);
  if(rv == -1){
    perror("main(): send()");
    return EXIT_FAILURE;
  }
  free(app_inf);

  //counter of the number of files written to
  int num_files_written_to = 0;
  //boolean which decides if open a new file or not
  int write_to_a_new_file = 1;
  //file size of the current file it receives
  uint16_t file_size = 0;
  //how many bytes of the receiving file that has arrived
  unsigned int bytes_received = 0;
  rv = 0;
  FILE* temp_file = NULL;

  /*
    This loop is controlled of blocking recv()-calls and distinguish whether it
    should continue to write to a open file or to open a new.
  */
  while(1){
    if(write_to_a_new_file == 1){
      rv = recv(sd, &file_size, 2, 0);
      if(rv == 0){
        printf("The transport daemon has performed an orderly shutdown\n");
        return EXIT_SUCCESS;
      } else if(rv == -1){
        perror("main(): recv():");
        return EXIT_FAILURE;
      }
      //printf("filstr: %u\n", (unsigned int)file_size);
      write_to_a_new_file = 0;
      temp_file = open_new_file(num_files_written_to, argv[2]);
      if(temp_file == NULL){
        close(sd);
        return EXIT_FAILURE;
      }
      num_files_written_to++;
      continue;
    }
    char buf[1492] = {0};
    if(file_size-bytes_received >= 1492){
      rv = recv(sd, buf, sizeof(buf), 0);
    } else{
      rv = recv(sd, buf, file_size-bytes_received, 0);
      buf[file_size-bytes_received] = '\0';
    }
    if(rv == 0){
      printf("The transport daemon has performed an orderly shutdown\n");
      return EXIT_SUCCESS;
    } else if(rv == -1){
      perror("main(): recv():");
      return EXIT_FAILURE;
    }

    int bytes_left = file_size - bytes_received;
    bytes_received += rv;

    if(rv == 1492){
      rv = fwrite(buf, sizeof(buf), sizeof(char), temp_file);
    } else{
      rv = fwrite(buf, bytes_left-1, sizeof(char), temp_file);
    }

    if(bytes_received >= file_size){
      write_to_a_new_file = 1;
      file_size = 0;
      bytes_received = 0;
      fclose(temp_file);
    }
  }
}
